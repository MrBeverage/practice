package org.beverage.practice.trees.tests;

import java.util.ArrayDeque;
import java.util.Deque;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class TraversalOrderTests {

    @Test(dataProvider = "preOrderTrees")
    public void preOrderIterative(final TreeNode head, final String expectedResult) {
        
        Deque<TreeNode> stack = new ArrayDeque<>();
        stack.push(head);
        
        StringBuilder actualResult = new StringBuilder();
        TreeNode current  = head;
        
        while (stack.size() > 0) {
            current = stack.pop();
            actualResult.append(current.getValue());
            
            if (current.getRight() != null) {
                stack.push(current.getRight());
            }
            if (current.getLeft() != null) {
                stack.push(current.getLeft());
            }
        }
        
        assertThat(actualResult.toString(), equalTo(expectedResult));
    }
    
    @Test(dataProvider = "preOrderTrees")
    public void preOrderRecursive(final TreeNode head, final String expectedResult) {
        String actualResult = preOrder(head);
        assertThat(actualResult, equalTo(expectedResult));
    }
    
    private String preOrder(final TreeNode head) {
        if (head == null) {
            return "";
        }
        
        String result = Integer.toString(head.getValue());
        
        result += preOrder(head.getLeft());
        result += preOrder(head.getRight());
        
        return result;
    }
    
    @Test(dataProvider = "inOrderTrees")
    public void inOrderIterative(final TreeNode head, final String expectedResult) {
        Deque<TreeNode> stack = new ArrayDeque<>();
        
        StringBuilder actualResult = new StringBuilder();
        TreeNode current  = head;

        while (current != null || stack.size() > 0) {
            if (current != null) {
                stack.push(current);
                current = current.getLeft();
            } else {
                TreeNode temp = stack.pop();
                actualResult.append(temp.getValue());
                current = temp.getRight();
            }
        }

        assertThat(actualResult.toString(), equalTo(expectedResult));
    }
    
    @Test(dataProvider = "inOrderTrees")
    public void inOrderRecursive(final TreeNode head, final String expectedResult) {
        String actualResult = inOrder(head);
        assertThat(actualResult, equalTo(expectedResult));
    }
    
    private String inOrder(final TreeNode head) {
        if (head == null) {
            return "";
        }
        
        String result = "";
        
        result += inOrder(head.getLeft());
        result += Integer.toString(head.getValue());
        result += inOrder(head.getRight());
        return result;
    }
    
    @Test(dataProvider = "inOrderTrees")
    public void inOrderNoSpace(final TreeNode head, final String expectedResult) {
        String actualResult = "";
        
        //  The test data generator sets the tree's head node to itself.  I'm sure there
        //  was some reason long ago for doing that that made sense at the time:
        head.setHead(null);
        
        TreeNode current  = head;
        TreeNode previous = null;
        
        while (current != null) {
            if (current.getLeft() != null && (previous == null || previous.getHead() != current)) {
                //  Traverse left:
                previous = current;
                current = current.getLeft();
            } else {
                //  Traverse center, but don't duplicate on right-backtrack:
                //  Special case for single-node trees.
                if (previous != current.getRight() || previous == null) {
                    actualResult += current.getValue();
                }
                
                //  Traverse right:
                if (current.getRight() != null && previous != current.getRight()) {
                    previous = current;
                    current = current.getRight();
                } else {
                    //  Backtracking:
                    previous = current;
                    current = current.getHead();
                }
            }
        }

        assertThat(actualResult, equalTo(expectedResult));
    }
    
    @Test(dataProvider = "postOrderTrees")
    public void postOrderIterative(final TreeNode head, final String expectedResult) {
        Deque<TreeNode> stack = new ArrayDeque<>();
        stack.push(head);
        
        StringBuilder actualResult = new StringBuilder();
        TreeNode current;
        TreeNode previous = null;
        
        while (stack.size() > 0) {
            current = stack.getFirst();
            
            if (previous == null || previous.getLeft() == current || previous.getRight() == current) {
                if (current.getLeft() != null) {
                    stack.push(current.getLeft());
                } else if (current.getRight() != null) {
                    stack.push(current.getRight());
                } else {
                    actualResult.append(current.getValue());
                    stack.pop();
                }
            } else if (current.getLeft() == previous) {
                if (current.getRight() != null) {
                    stack.push(current.getRight());
                } else {
                    actualResult.append(current.getValue());
                    stack.pop();
                }
            } else {
                actualResult.append(current.getValue());
                stack.pop();
            }
            
            previous = current;
        }
        
        assertThat(actualResult.toString(), equalTo(expectedResult));
    }
    
    @Test(dataProvider = "postOrderTrees")
    public void postOrderRecursive(final TreeNode head, final String expectedResult) {
        String actualResult = postOrder(head);
        assertThat(actualResult, equalTo(expectedResult));
    }
    
    private String postOrder(final TreeNode head) {
        if (head == null) {
            return "";
        }
        
        String result = "";
        
        result += postOrder(head.getLeft());
        result += postOrder(head.getRight());
        result += Integer.toString(head.getValue());
        return result;
    }
    
    @DataProvider
    public Object[][] preOrderTrees() {
        return new Object[][] {
                {  buildFullTree(0), "0" },
                {  buildFullTree(0).setLeft(new TreeNode(1)), "01" },
                {  buildFullTree(0).setRight(new TreeNode(1)), "01" },
                {  buildFullTree(1), "012" },
                {  buildFullTree(1).getLeft().setLeft(new TreeNode(3)).getHead(), "0132" },
                {  buildFullTree(1).getLeft().setRight(new TreeNode(3)).getHead(), "0132" },
                {  buildFullTree(1).getRight().setLeft(new TreeNode(3)).getHead(), "0123" },
                {  buildFullTree(1).getRight().setRight(new TreeNode(3)).getHead(), "0123" },
                {  buildFullTree(2), "0123456" },
        };
    }
    
    @DataProvider
    public Object[][] inOrderTrees() {
        return new Object[][] {
                {  buildFullTree(0), "0" },
                {  buildFullTree(0).setLeft(new TreeNode(1)), "10" },
                {  buildFullTree(0).setRight(new TreeNode(1)), "01" },
                {  buildFullTree(1), "102" },
                {  buildFullTree(1).getLeft().setLeft(new TreeNode(3)).getHead(), "3102" },
                {  buildFullTree(1).getLeft().setRight(new TreeNode(3)).getHead(), "1302" },
                {  buildFullTree(1).getRight().setLeft(new TreeNode(3)).getHead(), "1032" },
                {  buildFullTree(1).getRight().setRight(new TreeNode(3)).getHead(), "1023" },
                {  buildFullTree(2), "2130546" }
        };
    }
    
    @DataProvider
    public Object[][] postOrderTrees() {
        return new Object[][] {
                {  buildFullTree(0), "0" },
                {  buildFullTree(0).setLeft(new TreeNode(1)), "10" },
                {  buildFullTree(0).setRight(new TreeNode(1)), "10" },
                {  buildFullTree(1), "120" },
                {  buildFullTree(1).getLeft().setLeft(new TreeNode(3)).getHead(), "3120" },
                {  buildFullTree(1).getLeft().setRight(new TreeNode(3)).getHead(), "3120" },
                {  buildFullTree(1).getRight().setLeft(new TreeNode(3)).getHead(), "1320" },
                {  buildFullTree(1).getRight().setRight(new TreeNode(3)).getHead(), "1320" },
                {  buildFullTree(2), "2315640" }
        };
    }
    
    public static TreeNode buildFullTree(final int maxDepth) {
        
        TreeNode head = new TreeNode(0);
        head.setHead(head);
        
        Deque<TreeNode> nodeStack = new ArrayDeque<>();
        nodeStack.push(head);
        
        int valueCounter = 1;
        int depthCounter = 0;
        
        while (nodeStack.isEmpty() == false) {
            TreeNode current = nodeStack.getFirst();
            
            if (depthCounter < maxDepth && current.getLeft() == null) {
                TreeNode newLeft = new TreeNode(valueCounter++);
                newLeft.setHead(head);
                current.setLeft(newLeft);
                nodeStack.push(newLeft);
                depthCounter++;
            } else if (depthCounter < maxDepth && current.getRight() == null){ 
                TreeNode newRight = new TreeNode(valueCounter++);
                newRight.setHead(head);
                current.setRight(newRight);
                nodeStack.push(newRight);
                depthCounter++;
            } else {
                nodeStack.pop();
                depthCounter--;
            }
        }
        
        return head;
    }
    
    /**
     * Test TreeNode object.  Contains an additional head pointer in every node to make
     * test data construction a bit easier.
     * 
     * @author alexbeverage
     */
    @Data
    @Accessors(chain = true)
    @AllArgsConstructor
    public static class TreeNode {
        int value;
        
        //  Store the head node for easy construction in @DataProviders.
        TreeNode head;
        TreeNode left;
        TreeNode right;
        
        public TreeNode setLeft(TreeNode node) {
            left = node;
            node.head = this;
            return this;
        }
        
        public TreeNode setRight(TreeNode node) {
            right = node;
            node.head = this;
            return this;
        }
        
        public TreeNode(int value) {
            this.value = value;
        }
        
        @Override
        public String toString() {
            return Integer.toString(value);
        }
    }
}
