package org.beverage.practice.problems.tests;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class RomanNumeralsTests {

    @Test(dataProvider = "testCases")
    public void test(final String romanNumber, final int expectedResult) {
        int result = convertFromRomanNumerals(romanNumber);
        
        assertThat(result, equalTo(expectedResult));
    }
    
    @DataProvider
    public static Object[][] testCases() {
        return new Object[][] {
                { "I", 1 },
                { "IV", 4 },
                { "V", 5 },
                { "IX", 9 },
                { "X", 10 },
                { "XX", 20 },
                { "CMXCIX", 999 },
                { "MMMM", 4000 },
                { "CMCM", 1800 },
        };
    }
    
    public int convertFromRomanNumerals(final String romanNumber) {
        
        int result = 0;
        
        for (int i=0; i<romanNumber.length(); i++) {
            int value = 0;
            
            int current = convertRomanNumeral(romanNumber.charAt(i));
            int next = i < romanNumber.length() - 1 ? convertRomanNumeral(romanNumber.charAt(i+1)) : 0;
            
            if (next > current) {
                value = next - current;
                i++;
            } else {
                value = current;
            }
            
            result += value;
        }
        
        return result;
    }
    
    public int convertRomanNumeral(char c) {
        switch (c) {
            case 'I':
            case 'i':
                return 1;
                
            case 'V':
            case 'v':
                return 5;
                
            case 'X':
            case 'x':
                return 10;
                
            case 'L':
            case 'l':
                return 50;
                
            case 'C':
            case 'c':
                return 100;
                
            case 'D':
            case 'd':
                return 500;
                
            case 'M':
            case 'm':
                return 1000;
            
            default:
                return 0;
        }
    }
}
