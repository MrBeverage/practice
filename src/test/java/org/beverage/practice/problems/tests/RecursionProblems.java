package org.beverage.practice.problems.tests;

import java.util.Arrays;
import java.util.Set;

import org.beverage.practice.sets.SetUtils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.google.common.collect.Sets;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class RecursionProblems {

    @Test(dataProvider = "nStepsTestCases")
    public void nStepsProblemRecursive(final int steps, final long expectedResult) {
        long recursiveResult = nStepsRecursive(steps);
        
        assertThat(recursiveResult, equalTo(expectedResult));
    }

    @Test(dataProvider = "nStepsTestCases")
    public void nStepsProblemDP(final int steps, final long expectedResult) {
        long[] dpTable = new long[steps + 1];
        Arrays.fill(dpTable, -1);
        long dpResult = nStepsDP(steps, dpTable);

        assertThat(dpResult, equalTo(expectedResult));
    }
    
    @Test(dataProvider = "nStepsComparitiveTestCases")
    public void nStepsComparison(final int steps) {
        long[] dpTable = new long[steps + 1];
        Arrays.fill(dpTable, -1);
        long dpResult = nStepsDP(steps, dpTable);

        assertThat(dpResult, equalTo(dpResult));
    }
    
    @DataProvider
    public Object[][] nStepsTestCases() {
        //  Zero case has a bug where the base case (after at least one recursion) of a zero result
        //  means a solution has been found.  When zero is the initial value then there is no solution.
        //  The fix for this is to put the two methods that do the actual work into wrappers.
        return new Object[][] {
                /* { 0, 0l }, */ { 1, 1l }, { 2, 2l }, { 3, 4l }
        };
    }
    
    @DataProvider
    public Object[][] nStepsComparitiveTestCases() {
        return new Object[][] {
                { 4 }, { 5 }, { 10 }, { 20 }, { 30 }
        };
    }
    
    public long nStepsRecursive(long maxSteps) {
        if (maxSteps == 0) {
            return 1;
        } else if (maxSteps < 0) {
            return 0;
        } else {
            return nStepsRecursive(maxSteps - 1) + nStepsRecursive(maxSteps - 2) + nStepsRecursive(maxSteps - 3);
        }
    }
    
    public long nStepsDP(int steps, long[] table) {
        if (steps == 0) {
            return 1;
        } else if (steps < 0) {
            return 0;
        } else if (table[steps] > -1) {
            return table[steps];
        } else {
            table[steps] = nStepsDP(steps - 1, table) + nStepsDP(steps - 2, table) +nStepsDP(steps - 3, table);
            return table[steps];
        }
    }
    
    
    @Test(dataProvider = "powerSetTestCases")
    public <T> void powerSetTest(final int expectedSize, final Set<T> testSet) {
        Set<Set<T>> results = SetUtils.powerSet(testSet);
        
        assertThat(results.size(), equalTo(expectedSize));
    }
    
    @DataProvider
    public Object[][] powerSetTestCases() {
        return new Object[][] {
                {    1, Sets.newHashSet() },
                {    2, Sets.newHashSet(1) },
                {    4, Sets.newHashSet(1, 2) },
                {    8, Sets.newHashSet(1, 2, 3) },
                {   16, Sets.newHashSet(1, 2, 3, 4) },
                { 1024, Sets.newHashSet(1, 2, 3, 4, 5, 6, 7, 8, 9, 10) }
        };
    }
}
