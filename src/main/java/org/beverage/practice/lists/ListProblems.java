package org.beverage.practice.lists;

import lombok.Data;
import lombok.experimental.Accessors;

public class ListProblems {
    
    
    @Data
    @Accessors(fluent = false, chain = true)
    public static class ListNode<T extends Comparable<T>> {
        T value;
        ListNode<T> next;
        
        public ListNode() {
            
        }
        
        public ListNode(final T value, final ListNode<T> next) {
            this.value = value;
            this.next = next;
        }
        
        public ListNode(final ListNode<T> toCopy) {
            this(toCopy.getValue(), toCopy.getNext());
        }

        @SuppressWarnings("unchecked")
        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            ListNode<T> other = (ListNode<T>) obj;
            if (value == null) {
                if (other.value != null)
                    return false;
            } else if (!value.equals(other.value))
                return false;
            return true;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((value == null) ? 0 : value.hashCode());
            return result;
        }
        
        @Override
        public String toString() {
            return value.toString();
        }
    }
    
    public static <T extends Comparable<T>> ListNode<T> copyList(final ListNode<T> list) {
        if (list == null) {
            return null;
        }
        
        ListNode<T> copyHead = new ListNode<>();
        copyHead.setValue(list.getValue());
        
        ListNode<T> listCurrent = list;
        ListNode<T> copyCurrent = copyHead;
        ListNode<T> temp;
        
        while (listCurrent.getNext() != null) {
            temp = new ListNode<>();
            temp.setValue(listCurrent.getNext().getValue());
            
            copyCurrent.setNext(temp);
            copyCurrent = copyCurrent.getNext();
            
            listCurrent = listCurrent.getNext();
        }
        
        return copyHead;
    }

    public static <T extends Comparable<T>> ListNode<T> getLastNode(ListNode<T> head) {
        ListNode<T> current = head;
        while (current.getNext() != null) {
            current = current.getNext();
        }
        return current;
    }

    public static <T extends Comparable<T>> ListNode<T> advanceNode(ListNode<T> node, int distance) {
        ListNode<T> current = node;
        for (int i=0; i<distance; i++) {
            current = current.getNext();
        }
        return current;
    }

    public static <T extends Comparable<T>> ListNode<T> reverseList(ListNode<T> head) {
        
        if (head == null) {
            return null;
        }
        
        if (head.getNext() == null) {
            return head;
        }
        
        ListNode<T> prev    = null;
        ListNode<T> current = head;
        ListNode<T> next    = null;
        
        while (current != null) {
            next = current.getNext();
            current.setNext(prev);
            prev = current;
            current = next;
        }
        
        return prev;
    }

    public static <T extends Comparable<T>> ListNode<T> zipList(ListNode<T> head) {
        
        if (head == null) {
            return null;
        }
        
        if (head.getNext() == null || head.getNext().getNext() == null) {
            return head;
        }
        
        ListNode<T> current = head;
        ListNode<T> half    = head;
        
        //  Advance the halfway pointer one for every two nodes.
        //  Give the next node to the half pointer as the second list is the 
        //  interleaved one.  (Would get 0,n,1...n/2,n/2-2,n/2-1 otherwise.)
        current = current.getNext();
        while (current != null) {
            current = current.getNext();
            if (current != null) {
                current = current.getNext();
            } 
            half = half.getNext();
        }
        
        current = head;
        half = reverseList(half);
        
        ListNode<T> lowerTemp = current.getNext();
        ListNode<T> upperTemp = half.getNext();
        
        while (upperTemp != null) {
            current.setNext(half);
            half.setNext(lowerTemp);
            current = lowerTemp;
            half = upperTemp;
            lowerTemp = lowerTemp.getNext();
            upperTemp = upperTemp.getNext();
        }
        
        return head;
    }

    public static <T extends Comparable<T>> ListNode<T> mergeSortedLists(final ListNode<T> leftHead, final ListNode<T> rightHead) {
        
        if (leftHead == null && rightHead != null) {
            return rightHead;
        } else if (rightHead == null && leftHead != null) {
            return leftHead;
        } else if (leftHead == null && rightHead == null) {
            return null;
        }
        
        ListNode<T> leftCurrent = leftHead;
        ListNode<T> rightCurrent = rightHead;
        
        ListNode<T> resultHead = null;
        ListNode<T> resultCurrent = null;
        
        if (leftHead.getValue().compareTo(rightHead.getValue()) <= 0) {
            resultHead = resultCurrent = leftHead;
            leftCurrent = leftCurrent.next;
        } else {
            resultHead = resultCurrent = rightHead;
            rightCurrent = rightCurrent.next;
        }
        
        
        while (leftCurrent != null && rightCurrent != null) {
            //  Define null value as less than anything else.
            if (leftCurrent == null || leftCurrent.getValue().compareTo(rightCurrent.getValue()) <= 0) {
                resultCurrent.setNext(leftCurrent);
                resultCurrent = resultCurrent.getNext();
                leftCurrent = leftCurrent.getNext();
            } else {
                resultCurrent.setNext(rightCurrent);
                resultCurrent = resultCurrent.getNext();
                rightCurrent = rightCurrent.getNext();
            }
        }
        
        //  Handle list remainders.
        if (leftCurrent != null) {
            resultCurrent.setNext(leftCurrent);
        } else if (rightCurrent != null) {
            resultCurrent.setNext(rightCurrent);
        }
        
        return resultHead;
    }
    
    public static <T extends Comparable<T>> int size(final ListNode<T> head) {
        if (head == null) {
            return 0;
        } else {
            ListNode<T> current = head;
            int size = 1;
            while (current.next != null) {
                size++;
                current = current.next;
            }
            return size;
        }
    }
    
    public static <T extends Comparable<T>> int compareTo(final ListNode<T> left, final ListNode<T> right) {
        int leftSize = size(left);
        int rightSize = size(right);
        
        if (leftSize < rightSize) {
            return -1;
        } else if (leftSize > rightSize) {
            return 1;
        } else {
            ListNode<T> currentLeft = left;
            ListNode<T> currentRight = right;
            
            while (currentLeft != null) {
                if (currentLeft.getValue().compareTo(currentRight.getValue()) == -1) {
                    return -1;
                } else if (currentLeft.getValue().compareTo(currentRight.getValue()) == 1) {
                    return 1;
                }
                
                currentLeft = currentLeft.getNext();
                currentRight = currentRight.getNext();
            }
            
            return 0;
        }
    }

    /**
     * Utility method to convert from AbstractList and children.  Makes @DataProvider methods easier
     * to write.
     * @param list
     * @return
     */
    public static <T extends Comparable<T>, L extends AbstractList<T>> ListNode<T> listToNode(L list) {
        
        ListNode<T> headNode = null;
        ListNode<T> current = null;
        
        for (T item : list) {
            ListNode<T> temp = new ListNode<T>(item, null);
            if (current == null) {
                headNode = current = temp;
            } else {
                current.next = temp;
                current = current.next;
            }
        }
        
        return headNode;
    }
    
    public static <T extends Comparable<T>> ListNode<T> findCycle(final ListNode<T> list) {
        if (list == null || list.next == null) {
            return null;
        }
        
        ListNode<T> slow = list;
        ListNode<T> fast = list;
        
        while (slow != null && fast != null) {
            slow = slow.next;
            fast = fast.next;
            
            if (fast == null) {
                return null;
            }
            
            fast = fast.next;
            
            if (slow == null || fast == null) {
                return null;
            }
            
            if (slow.equals(fast)) {
                //  We've found a cycle - find it's length:
                int cycleLength = 0;
                
                do {
                    slow = slow.next;
                    cycleLength++;
                } while (slow.equals(fast) == false);
                
                //  Find the origin:
                slow = fast = list; 
                fast = advanceNode(fast, cycleLength);
                
                while (slow.equals(fast) == false) {
                    slow = slow.next;
                    fast = fast.next;
                }
                
                return slow;
            }
        }
        
        //  Not sure it's even possible to reach here.
        return null;
    }
}


