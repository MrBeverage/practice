package org.beverage.practice.lists.test;

import java.security.SecureRandom;
import java.util.Iterator;
import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.beverage.practice.lists.List;
import org.beverage.practice.lists.SinglyLinkedList;
import org.testng.annotations.DataProvider;

public class ListTestData {

    public static int[] LIST_SIZES = new int[] { 0, 1, 9, 10, 50, 100 };
    private static SecureRandom random = new SecureRandom();
    
    @DataProvider(name = "listSizes")
    public static Iterator<Object[]> listSizes() {
        
        ArrayList<Object[]> testMethodArgs = new ArrayList<>();
        for (int size : LIST_SIZES) {
            testMethodArgs.add(new Object[] { size });
        }
        
        return testMethodArgs.iterator();
    }
    @DataProvider
    public static Object[][] lists() {
        return new Object[][] {
                { buildSinglyLinkedList(buildIntArray(10)) },
                { buildSinglyLinkedList(buildIntArray(9))  },
                { null },
                { buildSinglyLinkedList(new Integer[] { 1 }) },
        };
    }
    
    public static Integer[] buildIntArray(int maxValue) {
        Integer[] values = new Integer[maxValue];
        
        for (int i=0; i<maxValue; i++) {
            values[i] = i;
        }
        
        return values;
    }
    
    public static Integer[] buildRandomIntArray(int numItems, int maxValue) {
        Integer[] values = new Integer[numItems];
        
        for (int i=0; i<numItems; i++) {
            values[i] = random.nextInt(maxValue);
        }
        
        return values;
    }
    
    public static <T> SinglyLinkedList<T> buildSinglyLinkedList(T[] values) {
        
        SinglyLinkedList<T> list = new SinglyLinkedList<>();
        
        for (int i=0; i<values.length; i++) {
            list.add(values[i]);
        }
        
        return list;
    }
    
    public static <T> List<T> buildArrayList(List<T> list, T[] values) {
        for (T value : values) {
            list.add(value);
        }
        return list;
    }
    
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class DummyObject implements Comparable<DummyObject> {
        Integer number;
        String foo;
        DummyObject obj;

        @Override
        public int compareTo(DummyObject o) {
            return number.compareTo(o.number);
        }
    }
}
