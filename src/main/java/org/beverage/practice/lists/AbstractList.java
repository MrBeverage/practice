package org.beverage.practice.lists;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ConcurrentModificationException;
import java.util.Iterator;

public abstract class AbstractList<V> implements List<V>, Comparable<AbstractList<V>> {

    private BasicListNode<V> headNode;
    private int size;
    
    //  Discover once and only once if the ValueType used here is Comparable.
    //  These cannot be static as static members are shared across all generics 
    //  of their class.  Consider a simple static table for sharing instead later.
    boolean knowIfComparable;
    boolean valueTypeIsComparable;
    Method  valueCompareTo;
    
    //  Version number.  For tracking co-modification.
    int revisionNumber;
    
    protected AbstractList() {
        this.knowIfComparable = false;
        this.valueTypeIsComparable = false;
        this.valueCompareTo = null;
        this.revisionNumber = 0;
    }
    
    @Override
    public void clear() {
        if (headNode == null) {
            return;
        }
        
        //  Should probably do the right thing and nullify everything for GC's sake.
        BasicListNode<V> current = headNode;
        BasicListNode<V> previous = null;
        
        while (current != null) {
            incrementRevisionNumber();
            
            previous = current;
            current = current.next;
            
            previous.next = null;
            previous = null;
            
            decrementSize();
        }
    }
    
    @Override
    public boolean contains(V value) {
        for (V element : this) {
            if (element.equals(value)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<V> iterator() {
        return new ListIterator(getHeadNode());
    }
    
    @Override
    public int indexOf(V value) {
        int location = -1;
        for (V item : this) {
            location++;
            if (item.equals(value)) {
                break;
            }
        }
        return location;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        for (V item : this) {
            result = prime * item.hashCode() + (item == null ? 0 : item.hashCode());
        }
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        
        if (other.getClass().isAssignableFrom(List.class) == false) {
            return false;
        }
        
        @SuppressWarnings("unchecked")
        List<V> otherList = (List<V>) other;
        
        if (this.size != otherList.size()) {
            return false;
        }
        
        if (this.getClass().equals(other.getClass()) == false) {
            return false;
        }
        
        Iterator<V> thisIterator = this.iterator();
        Iterator<V> otherIterator = otherList.iterator();

        while (thisIterator.hasNext() && otherIterator.hasNext()) {
            if (thisIterator.next().equals(otherIterator.next()) == false) {
                return false;
            }
        }
        
        return true;
    }
    
    @Override
    public int compareTo(AbstractList<V> other) {
        
        //  Short circuit on the size differences immediately.
        if (this.size < other.size) {
            return -1;
        } else if (this.size > other.size) {
            return 1;
        }
        
        Iterator<V> thisIterator = this.iterator();
        Iterator<V> otherIterator = other.iterator();
        
        int compareToResult;
        
        while (thisIterator.hasNext() && otherIterator.hasNext()) {
            V left  = thisIterator.next();
            V right = otherIterator.next();
            
            //  Fucking hell.  Why did I decide that I needed to make Lists comparable
            //  in the beginning?  Oh well, let's cache the comparability of the value
            //  type just once to keep the performance from tanking.  I refuse to delete
            //  all the damn tests I wrote for this.  This is so much work for such a
            //  rare case...
            if (knowIfComparable == false && Comparable.class.isAssignableFrom(left.getClass())) {
                try {
                    valueCompareTo = left.getClass().getMethod("compareTo", left.getClass());
                    knowIfComparable = true;
                    valueTypeIsComparable = true;
                } catch (NoSuchMethodException | SecurityException e) {
                    throw new RuntimeException(e);
                }
            }
            
            if (valueTypeIsComparable) {
                //  The performance difference of this is actually quite negliable in 
                //  newer versions of java.  Benchmarks have been shown online to prove it.
                try {
                    compareToResult = (int) valueCompareTo.invoke(left, right);
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                    throw new RuntimeException(e);
                }
                if (compareToResult != 0) {
                    return compareToResult;
                }
            } else {
                if (left.equals(right) == false) {
                    return this.size > other.size ? 1 : -1;
                }
            }
        }

        return 0;
    }

    public int size() {
        return size;
    }
    
    protected void setSize(int size) {
        this.size = size;
    }

    protected void incrementSize() {
        this.size++;
    }

    protected void decrementSize() {
        this.size--;
    }
    
    /**
     * Increments the version number of this list.
     * @return The new version number.
     */
    protected int incrementRevisionNumber() {
        return ++this.revisionNumber;
    }
    
    /**
     * Checks that the list has not been modified by some concurrent process by comparing
     * an expected revision number against the internal revision.  This is to help ensure
     * (but technically not guarantee) thread safety on reads, and non-interference during
     * iteration.  {@link ConcurrentModificationException}  There is still the possibility
     * (less likely now) of race conditions and completely thread safety should not be
     * assumed.
     * @param expectedRevision The expected revision number.
     * @throws ConcurrentModificationException if the list has been modified.
     */
    protected void checkForComodification(int expectedRevision) {
        if (this.revisionNumber != expectedRevision) {
            throw new ConcurrentModificationException("List was modified while iterating through it.");
        }
    }
    
    public BasicListNode<V> getHeadNode() {
        //  Made public for practice problems later.
        return headNode;
    }

    public void setHeadNode(BasicListNode<V> headNode) {
        //  Made public for practice problems later.
        this.headNode = headNode;
    }
    
    protected abstract BasicListNode<V> getNode(int index);

    public static class BasicListNode<T> {
        T value;
        BasicListNode<T> next;
    }

    /**
     * It is seeming more and more likely that each individual {@link List} specialization
     * is going to need its own custom iterator.  Remove this later when that happens.
     * 
     * @author alexbeverage
     */
    protected class ListIterator implements Iterator<V> {

        private BasicListNode<V> current;
        private BasicListNode<V> previous;
        private int cursor;
        private int lastReturned;
        private int expectedRevision;
        
        public ListIterator(BasicListNode<V> value) {
            this.current = null;
            this.previous = null;
            this.cursor = 0;
            this.lastReturned = -1;
            this.expectedRevision = revisionNumber;
        }

        @Override
        public boolean hasNext() {
            return cursor < size();
        }

        @Override
        public V next() {
            AbstractList.this.checkForComodification(expectedRevision);
            V value = null;
            if (current == null && cursor == 0) {
                current = getHeadNode();
                value = current.value;
            } else if (current == null) {
                throw new IllegalStateException(
                        "Have already iterated through the list - did you forget to call hasNext()?  " + cursor);
            } else {
                previous = current;
                current = current.next;
                value = current.value;
            }
            cursor++;
            lastReturned++;
            return value;
        }
        
        @Override
        public void remove() {
            AbstractList.this.checkForComodification(expectedRevision);
            expectedRevision++;
            
            if (lastReturned != 0) {
                //  Removing any middle or the end node.
                previous.next = current.next;
            } else if (lastReturned == 0 && current.next != null) {
                //  Removing first node - has subsequent nodes.
                previous = current;
                AbstractList.this.setHeadNode(current.next);
                current = null;
                
                //  Null out previous node for GC.
                previous.next = null;
                previous = null;
            } else {
                //  Removing first node - no subsequent nodes.
                current.next = null;
                current = null;
            }
            
            cursor--;
            lastReturned--;
            
            AbstractList.this.decrementSize();
            AbstractList.this.incrementRevisionNumber();
            expectedRevision = AbstractList.this.revisionNumber;
        }
    }
}
