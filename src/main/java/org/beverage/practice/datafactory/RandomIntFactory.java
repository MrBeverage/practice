package org.beverage.practice.datafactory;

import java.security.SecureRandom;

public class RandomIntFactory implements RandomObjectFactory<Integer> {
    
    static final SecureRandom random = new SecureRandom();
    
    private final int upperBound;
    
    public RandomIntFactory() {
        this(Integer.MAX_VALUE);
    }
    
    public RandomIntFactory(int upperBound) {
        this.upperBound = upperBound;
    }
    
    @Override
    public Integer next() {
        return random.nextInt(upperBound);
    }
}
