package org.beverage.practice.datafactory;

/**
 * Interface to describe objects that are capable of generating random test data.
 * 
 * @author alexbeverage
 *
 * @param <T>
 */
public interface RandomObjectFactory<T> {
    T next();
}
