package org.beverage.practice.lists.test;

import java.security.SecureRandom;
import java.util.ConcurrentModificationException;

import org.beverage.practice.lists.List;
import org.beverage.practice.lists.SinglyLinkedList;
import org.beverage.practice.lists.test.ListTestData.DummyObject;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class SingleLinkedListTests {

    private static SecureRandom random = new SecureRandom();

    @Test(dataProvider = "listSizes", dataProviderClass = ListTestData.class)
    public void createMultiItemNumericList(int listSize) {
        SinglyLinkedList<Integer> list = new SinglyLinkedList<>();

        for (int i = 0; i < listSize; i++) {
            list.add(i);
        }

        assertThat(list.size(), equalTo(listSize));

        for (int i = 0; i < listSize; i++) {
            assertThat(list.get(i), equalTo(i));
        }
    }

    @Test(dataProvider = "listSizes", dataProviderClass = ListTestData.class)
    public void numbericListIterator(int listSize) {
        SinglyLinkedList<Integer> list = new SinglyLinkedList<>();

        for (int i = 0; i < listSize; i++) {
            list.add(i);
        }

        assertThat(list.size(), equalTo(listSize));

        for (Integer i : list) {
            assertThat(list.get(i), equalTo(i));
        }
    }
    
    private SinglyLinkedList<DummyObject> buildDummyObjectArray(int size) {
        SinglyLinkedList<DummyObject> list = new SinglyLinkedList<>();
        for (int i=0; i<size; i++) {
            list.add(new DummyObject(i, null, null));
        }
        return list;
    }

    @Test(dataProvider = "listSizes", dataProviderClass = ListTestData.class)
    public void createMultiItemObjectList(int listSize) {
        SinglyLinkedList<DummyObject> list = new SinglyLinkedList<>();

        for (int i = 0; i < listSize; i++) {
            list.add(new DummyObject(i, null, null));
        }

        assertThat(list.size(), equalTo(listSize));

        for (int i = 0; i < listSize; i++) {
            assertThat(list.get(i).getNumber(), equalTo(i));
        }
    }

    @Test(dataProvider = "listSizes", dataProviderClass = ListTestData.class)
    public void compareToLessThan(int listSize) {

        SinglyLinkedList<DummyObject> leftList = new SinglyLinkedList<>();
        SinglyLinkedList<DummyObject> rightList = new SinglyLinkedList<>();

        for (int i = 0; i < listSize; i++) {
            leftList.add(new DummyObject(i, null, null));
            rightList.add(new DummyObject(i, null, null));
        }

        // Skip the listSize == 0 case - it makes no sense for the removal
        // check.
        if (listSize != 0) {
            // Non-null second list:
            rightList.remove(rightList.size() - 1);
            assertThat(rightList.compareTo(leftList), equalTo(-1));

            int size = rightList.size();
            for (int i = 0; i < size; i++) {
                rightList.remove(0);
            }

            assertThat(rightList.size(), equalTo(0));
            assertThat(rightList.compareTo(leftList), equalTo(-1));
        }
    }

    @Test(dataProvider = "listSizes", dataProviderClass = ListTestData.class)
    public void compareToGreaterThan(int listSize) {

        SinglyLinkedList<DummyObject> leftList = new SinglyLinkedList<>();
        SinglyLinkedList<DummyObject> rightList = new SinglyLinkedList<>();

        for (int i = 0; i < listSize; i++) {
            leftList.add(new DummyObject(i, null, null));
            rightList.add(new DummyObject(i, null, null));
        }

        // Skip the listSize == 0 case - it makes no sense for the removal
        // check.
        if (listSize != 0) {
            // Non-null second list:
            rightList.add(new DummyObject(rightList.size(), null, null));
            assertThat(rightList.compareTo(leftList), equalTo(1));

            int size = leftList.size();
            for (int i = 0; i < size; i++) {
                leftList.remove(0);
            }

            assertThat(leftList.size(), equalTo(0));
            assertThat(rightList.compareTo(leftList), equalTo(1));
        }
    }

    @Test(dataProvider = "listSizes", dataProviderClass = ListTestData.class)
    public void compareToEquality(int listSize) {

        SinglyLinkedList<DummyObject> leftList = new SinglyLinkedList<>();
        SinglyLinkedList<DummyObject> rightLists = new SinglyLinkedList<>();

        for (int i = 0; i < listSize; i++) {
            leftList.add(new DummyObject(i, null, null));
            rightLists.add(new DummyObject(i, null, null));
        }

        // Non-null second list:
        assertThat(leftList.size(), equalTo(rightLists.size()));
        assertThat(leftList.compareTo(rightLists), equalTo(0));
    }
    
    @Test(dataProvider = "listSizes", dataProviderClass = ListTestData.class) 
    public void insert(int listSize) {
        if (listSize != 0) {
            int insertAt = random.nextInt(listSize);
            SinglyLinkedList<DummyObject> list = this.buildDummyObjectArray(listSize);
            
            list.insert(new DummyObject(-1, null, null), insertAt);
            
            //  Check size match first to avoid occasional boundary-induced NPEs.
            assertThat(list.size(), equalTo(listSize + 1));
            assertThat(list.get(insertAt).getNumber(), equalTo(-1));
            assertThat(list.get(insertAt - 1).getNumber(), equalTo(insertAt - 1));
            assertThat(list.get(insertAt + 1).getNumber(), equalTo(insertAt));
        } else {
            SinglyLinkedList<DummyObject> list = new SinglyLinkedList<>();
            list.insert(new DummyObject(-1, null, null), 0);
            
            assertThat(list.size(), equalTo(1));
            assertThat(list.get(0).getNumber(), equalTo(-1));
        }
    }
    
    @Test(expectedExceptions = ConcurrentModificationException.class)
    public void coModificationAdd() {
        List<Integer> list = new SinglyLinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        
        for (int value : list) {
            if (value == 2) {
                list.add(4);
            }
        }
    }

    @Test(expectedExceptions = ConcurrentModificationException.class)
    public void coModificationRemove() {
        List<Integer> list = new SinglyLinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        
        for (int value : list) {
            if (value == 1) {
                list.remove(2);
            }
        }
    }
    
    @Test(expectedExceptions = ConcurrentModificationException.class)
    public void coModificationInsert() {
        List<Integer> list = new SinglyLinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        
        for (int value : list) {
            if (value == 2) {
                list.insert(0, 0);
            }
        }
    }
    
    @Test
    public void canClear() {
        SinglyLinkedList<Integer> list = ListTestData.buildSinglyLinkedList(ListTestData.buildIntArray(100));
        list.clear();
        
        assertThat(list.size(), equalTo(0));
    }
}
