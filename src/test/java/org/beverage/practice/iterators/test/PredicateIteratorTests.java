package org.beverage.practice.iterators.test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.beverage.practice.iterators.PredicateIterator;
import org.beverage.practice.lists.test.ListTestData;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Predicate;

public class PredicateIteratorTests {

    private static final SecureRandom random = new SecureRandom();
    
    @Test(dataProvider = "iterateTestCases")
    public <T> void iterate(T[] items, Predicate<T> predicate) {
        
        Collection<T> expectedResult = new ArrayList<>();
        buildExpectedResult(expectedResult, items, predicate);
        
        Collection<T> actualResult = new ArrayList<>();
        
        //  PredicateIterator uses BeverageCollections' Collection interface.
        PredicateIterator<T> iterator = new PredicateIterator<>(
                new org.beverage.practice.lists.ArrayList<>(items), predicate);
        
        while (iterator.hasNext()) {
            actualResult.add(iterator.next());
        }
        
        assertThat(actualResult.size(), equalTo(expectedResult.size()));
        
        if (expectedResult.size() > 0) {
            assertThat(actualResult, contains(expectedResult.toArray()));
        }
    }
    
    @Test(dataProvider = "removeTestCases")
    public <T> void remove(T[] items, T itemToRemove, Predicate<T> predicate) {
        Collection<T> expectedResult = new ArrayList<>();
        buildExpectedResult(expectedResult, items, predicate);
        expectedResult.remove(itemToRemove);
        
        Collection<T> actualResult = new ArrayList<>();
        
        //  PredicateIterator uses BeverageCollections' Collection interface.
        org.beverage.practice.collections.Collection<T> collection = 
                new org.beverage.practice.lists.ArrayList<>(items);
        
        PredicateIterator<T> iterator = new PredicateIterator<>(collection, predicate);
        
        while (iterator.hasNext()) {
            T nextItem = iterator.next();
            
            if (nextItem.equals(itemToRemove)) {
                iterator.remove();
            } else {
                actualResult.add(nextItem);
            }
        }
        
        assertThat(actualResult.size(), equalTo(expectedResult.size()));
        
        if (expectedResult.size() > 0) {
            assertThat(actualResult, contains(expectedResult.toArray()));
        }
        
        if (predicate.test(itemToRemove) == true) {
            //  Assert that the targeted item was removed from the underlying collection
            //  if it passes the predicate's test.
            assertThat(collection.size(), equalTo(items.length - 1));
            assertThat(collection, not(hasItem(itemToRemove)));
            assertThat(actualResult, not(hasItem(itemToRemove)));
        } else {
            //  Assert that the targeted item was not removed from the underlying collection
            //  if it does not pass the predicate's test.
            assertThat(collection.size(), equalTo(items.length));
            assertThat(collection, hasItem(itemToRemove));
        }
    }
    
    public static class EvenPredicate<T> implements Predicate<Integer> {
        @Override
        public boolean test(Integer t) {
            return t % 2 == 0;
        }
    }
    
    private <T> void buildExpectedResult(Collection<T> expectedResults, T[] input, Predicate<T> predicate) {
        for (T i : input) {
            if (predicate.test(i)) {
                expectedResults.add(i);
            }
        }
    }
    
    @DataProvider
    private static Object[][] iterateTestCases() {
        //  Input - Predicate - Expected Result
        return new Object[][] { 
                { new Integer[] { 1 },            new EvenPredicate<Integer>() },
                { ListTestData.buildIntArray(10), new EvenPredicate<Integer>() }
        };
    }
    
    @DataProvider
    private static Object[][] removeTestCases() {
        //  Input - Predicate - Expected Result
        return new Object[][] { 
                { new Integer[] { 1 },            1,  new EvenPredicate<Integer>() },
                { ListTestData.buildIntArray(10), 0,  new EvenPredicate<Integer>() },
                { ListTestData.buildIntArray(11), 10, new EvenPredicate<Integer>() },
                { ListTestData.buildIntArray(10), 2 * random.nextInt(3) + 2,  new EvenPredicate<Integer>() },
        };
    }
}
