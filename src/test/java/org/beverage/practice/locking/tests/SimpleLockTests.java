package org.beverage.practice.locking.tests;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import lombok.Data;

import org.beverage.practice.locking.LockProvider;
import org.beverage.practice.locking.SimpleLock;
import org.testng.annotations.Test;

public class SimpleLockTests {
    
    private static final int ITERATIONS = 10;
    
    @Data
    public static class TestStateObject {
        int value;
        
        public TestStateObject(final int value) {
            this.value = value;
        }
    }
    
    @Data
    public abstract class LockingTestCallable<T> implements Callable<T> {

        final LockProvider lock;
        final TestStateObject stateObject;
        
        public LockingTestCallable(final LockProvider lock, final TestStateObject stateObject) {
            this.lock = lock;
            this.stateObject = stateObject;
        }
    }
    
    @Test(threadPoolSize = 10, invocationCount = ITERATIONS)
    public void taskTwoBetweenTasksOneAndThree() throws InterruptedException {
        int startValue = 10;
        int firstValue = 20;
        int secondValue = 30;
        int expectedValue = 50;
        
        long blockingSleep = 2000l;
        long startupDelay = 500l;
        
        SimpleLock lock = new SimpleLock();
        TestStateObject result = new TestStateObject(startValue);
        ExecutorService service = Executors.newFixedThreadPool(3);
        
        long startTime = System.currentTimeMillis();
        
        LockingTestCallable<Void> callableA = new LockingTestCallable<Void>(lock, result) {
            @Override
            public Void call() throws Exception {
                assertThat(lock.isLocked(), equalTo(false));
                lock.acquire();
                Thread.sleep(blockingSleep);
                stateObject.setValue(firstValue);
                lock.release();
                
                //  Let callableB acquire the lock.
                Thread.sleep(2* startupDelay);
                
                //  Set the final value once the lock is re-acquired.
                assertThat(lock.isLocked(), equalTo(true));
                lock.acquire();
                stateObject.setValue(expectedValue);
                lock.release();
                
                return null;
            }
        };
        
        LockingTestCallable<Void> callableB = new LockingTestCallable<Void>(lock, result) {
            @Override
            public Void call() throws Exception {
                assertThat(lock.isLocked(), equalTo(true));
                lock.acquire();
                Thread.sleep(blockingSleep);
                stateObject.setValue(secondValue);
                lock.release();
                return null;
            }
        };
        
        Future<Void> futureA = service.submit(callableA);
        Thread.sleep(startupDelay);
        Future<Void> futureB = service.submit(callableB);
        
        while (futureA.isDone() == false && futureB.isDone() == false) {
            Thread.sleep(100);
        }
        
        long runTime = System.currentTimeMillis() - startTime;
        
        assertThat(result.getValue(), equalTo(expectedValue));
        assertThat(runTime, greaterThan(blockingSleep));
    }
    
    @Test(threadPoolSize = 10, invocationCount = ITERATIONS)
    public void taskOneBlocksTaskTwo() throws InterruptedException {
        
        int startValue = 10;
        int blockedValue = 20;
        int expectedValue = 30;
        
        long blockingSleep = 2000l;
        long startupDelay = 500l;
        
        SimpleLock lock = new SimpleLock();
        TestStateObject result = new TestStateObject(startValue);
        ExecutorService service = Executors.newFixedThreadPool(3);
        
        long startTime = System.currentTimeMillis();
        
        LockingTestCallable<Void> callableA = new LockingTestCallable<Void>(lock, result) {
            @Override
            public Void call() throws Exception {
                assertThat(lock.isLocked(), equalTo(false));
                lock.acquire();
                Thread.sleep(blockingSleep);
                stateObject.setValue(blockedValue);
                lock.release();
                return null;
            }
        };
        
        LockingTestCallable<Void> callableB = new LockingTestCallable<Void>(lock, result) {
            @Override
            public Void call() throws Exception {
                assertThat(lock.isLocked(), equalTo(true));
                lock.acquire();
                stateObject.setValue(expectedValue);
                lock.release();
                return null;
            }
        };
        
        Future<Void> futureA = service.submit(callableA);
        Thread.sleep(startupDelay);
        Future<Void> futureB = service.submit(callableB);
        
        while (futureA.isDone() == false && futureB.isDone() == false) {
            Thread.sleep(100);
        }
        
        long runTime = System.currentTimeMillis() - startTime;
        
        assertThat(result.getValue(), equalTo(expectedValue));
        assertThat(runTime, greaterThan(blockingSleep));
    }
}
