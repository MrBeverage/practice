package org.beverage.practice.util;

public class MiscUtils {

    public static <T> void swap(T a, T b) {
        T temp = a;
        a = b;
        b = temp;
    }
}
