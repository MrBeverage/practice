package org.beverage.practice.problems.tests;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class ArrayProblems {

    @Test(dataProvider = "testCases")
    public void test(final long expectedSum, final int subsequenceLength, final int[] array) {
        long sum = bestSubsequenceSum(array, subsequenceLength);
        
        assertThat(sum, equalTo(expectedSum));
    }
    
    @DataProvider
    public Object[][] testCases() {
        return new Object[][] { 
                { 0l, 1, new int[] { } },
                { 1l, 1, new int[] { 1 } },
                { 2l, 1, new int[] { 1, 2, } },
                { 60l, 3, new int[] { 10, 20, 30, 10, 20 } },
                { 60l, 3, new int[] { 0, 20, 30, 10, 20 } },
                { 60l, 3, new int[] { 0, 0, 30, 10, 20 } },
                { 3l * (long)Integer.MAX_VALUE, 3, new int[] { Integer.MAX_VALUE - 2, Integer.MAX_VALUE - 1, Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE } },
        };
    }
    
    long bestSubsequenceSum(int[] array, int subseqLength) {
        
        //  Input checking here:
        long bestSum = 0l;
        
        for (int i=0; i<subseqLength && i<array.length; i++) {
            bestSum += array[i];
        }
        
        if (array.length > subseqLength) {
            long currentSum = bestSum;
            
            for (int i=subseqLength; i<array.length; i++) {
                currentSum -= (long) array[i - subseqLength];
                currentSum += (long) array[i];
                
                if (currentSum > bestSum) {
                    bestSum = currentSum;
                }
            }
        }
        
        return bestSum;
    }
}
