package org.beverage.practice.graphing.tests;

import java.util.Deque;
import java.util.List;

import org.beverage.practice.graphs.DirectedAcyclicGraph;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.google.common.collect.Lists;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class DirectedAcyclicGraphTests {

    @Test(dataProvider = "graphs")
    public void topologicalOrdering(final String name, 
            final DirectedAcyclicGraph<Integer, Integer> graph, final List<Integer> expectedOrdering) {
        
        Deque<Integer> result = graph.getTopologicalOrdering();
        
        if (expectedOrdering.size() > 0) {
            assertThat(result, contains(expectedOrdering.toArray()));
        } else {
            assertThat(result.isEmpty(), equalTo(true));
        }
    }
    
    @Test(dataProvider = "graphsWithCycles", expectedExceptions = IllegalStateException.class)
    public void cycleDetection(final String name, final DirectedAcyclicGraph<Integer, Integer> graph) {
        graph.getTopologicalOrdering();
    }
    
    @DataProvider
    public Object[][] graphs() {
        return new Object[][] {
                { "Empty",         dag(),                                   al() },
                { "0 - > 1",       dag().addEdge(0, 1, 0),                  al(1, 0) },
                { "0, 1 -> 2",     dag().addEdge(0, 2, 0).addEdge(1, 2, 0), al(2, 1, 0) },
                { "2 -> 1 -> 0",   dag().addEdge(2, 1, 0).addEdge(1, 0, 0), al(0, 1, 2) },
                { "Forest",        dag().addEdge(0, 1, 0).addEdge(2, 3, 0), al(3, 2, 1, 0) },
                { "Bigger forest", dag().addEdge(0, 1, 0).addEdge(2, 3, 0)
                                        .addEdge(3, 5, 0).addEdge(4, 5, 0)
                                        .addEdge(5, 6, 0).addEdge(8, 9, 0), al(9, 8, 6, 5, 4, 3, 2, 1, 0)
                }
        };
    }
    
    @DataProvider
    public Object[][] graphsWithCycles() {
        return new Object[][] {
                { "Simple Cycle", dag().addEdge(0, 1, 0).addEdge(1, 0, 0) },
                { "Forest Cycle", dag().addEdge(0, 1, 0).addEdge(2, 3, 0)
                                       .addEdge(3, 4, 0).addEdge(4, 2, 0) },
        };
    }
    
    //  Testdata helper methods:
    private DirectedAcyclicGraph<Integer, Integer> dag() {
        return new DirectedAcyclicGraph<Integer, Integer>();
    }
    
    @SuppressWarnings("unchecked")
    private <T> List<T> al(T...elements) {
        return Lists.newArrayList(elements);
    }
}
