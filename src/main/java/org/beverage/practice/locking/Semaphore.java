package org.beverage.practice.locking;

public class Semaphore {

    private boolean isNotified = false;
    
    public synchronized void doWait() throws InterruptedException {
        while (isNotified == false) {
            wait();
        }
        isNotified = false;
    }
    
    public synchronized void doNotify() {
        isNotified = true;
        notify();
    }
    
    @Override
    public boolean equals(Object other) {
        return this == other;
    }
}
