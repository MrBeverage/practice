package org.beverage.practice.primitives.test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.beverage.practice.primitives.BitwiseOperations;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class BitwiseOperationsTests {

    SecureRandom random = new SecureRandom();
    
    @Test(dataProvider = "additionProblems")
    public void addition(final long lh, final long rh, final long expectedResult) {
        long result = BitwiseOperations.add(lh, rh);
        
        assertThat(result, equalTo(expectedResult));
    }
    
    @DataProvider
    public Object[][] additionProblems() {
        return new Object[][] {
                { 0, 0, 0 }, { 1, 0, 1 }, { 0, 1, 1 }, { 1, 1, 2 }, { -1, 1, 0 }, { 1, -1, 0 }, { -1, -1, -2 },
                { Long.MAX_VALUE, Long.MAX_VALUE, -2 }, { Long.MIN_VALUE + 1, Long.MAX_VALUE, 0 }
        };
    }
    
    @Test(dataProvider = "divisionProblems")
    public void division(final long dividend, final long divisor, final long expectedResult) {
        long quotient = BitwiseOperations.divide(dividend, divisor);
        
        assertThat(quotient, equalTo(expectedResult));
    }
    
    @Test(dataProvider = "randomDivisionProblems") 
    public void randomDivision(final long dividend, final long divisor, final long expectedResult) {
        division(dividend, divisor, expectedResult);
    }
    
    @DataProvider
    public Object[][] divisionProblems() {
        return new Object[][] { 
                { 1, 1, 1 }, { 2, 2, 1 }, { 4, 2, 2}, { 16, 4, 4 }, { 32, 4, 8 },
                { 1, -1, -1 }, { 2, -2, -1 }, { 4, -2, -2}, { 16, -4, -4 }, { 32, -4, -8 },
                { 0b10101, 0b100, 0b101 }, { 0b10000, 0b10100, 0b0 },
                { 21, -4, -5 }, { 16, -24, 0 }, { -21, 4, -5 }, { -16, 24, 0 }, { -21, -4, 5 }, { -16, -24, 0 },
                { Long.MAX_VALUE, Long.MAX_VALUE, 1 }, { Long.MAX_VALUE, Long.MIN_VALUE + 1, -1 }, { Long.MAX_VALUE, Long.MIN_VALUE, 0 } };
    }
    
    @DataProvider
    public Iterator<Object[]> randomDivisionProblems() {
        List<Object[]> testCases = new ArrayList<Object[]>();
        
        long divisor = 0l;
        long dividend = 0l;
        
        for (int i=0; i<100; i++) {
            //  This will not overflow as the random number generator's seed is only 48 bits.
            //  Cases are more interesting when there are order-of-magnitude differences.
            //  We have 64 - 48 - 1 (sign) - 1 (zero-offset) bits to play with for that.  (10^7)
            divisor  = (random.nextLong() + 1) * (random.nextBoolean() == true ? 1 : -1);
            dividend = (random.nextLong() + 1) * (random.nextBoolean() == true ? 1 : -1);
            
            dividend *= random.nextLong() % 4194304;
            divisor  /= random.nextLong() % 4194304;
            
            testCases.add(new Object[] { dividend, divisor, dividend / divisor });
        }
        
        return testCases.iterator();
    }
    
    //  5.2
    @Test(dataProvider = "bitswappedLongs")
    public void swapBits(final long initial, final long expected,
            final int index1, final int index2) {
        long result = BitwiseOperations.swapBits(initial, index1, index2);

        assertThat(result, equalTo(expected));
    }

    //  5.3
    @Test(dataProvider = "reversedLongs")
    public void reverseBits(final long initial, final long expected) {
        long result = BitwiseOperations.reverse(initial);
        assertThat(result, equalTo(expected));
    }

    @DataProvider
    private static Object[][] bitswappedLongs() {
        return new Object[][] {
                { 0b00010000, 0b00000001, 4, 0 },
                { 0b00010000, 0b00010000, 2, 3 },
                { 0b11111111, 0b11111111, 1, 7 },
                { 0b00000000, 0b00000000, 3, 6 },
                {
                        0b0000000000000000000000000000000000000000000000000000000000000000L,
                        0b0000000000000000000000000000000000000000000000000000000000000000L,
                        0, 63 },
                {
                        0b1000000000000000000000000000000000000000000000000000000000000000L,
                        0b0000000000000000000000000000000000000000000000000000000000000001L,
                        0, 63 },
                {
                        0b1000000000000000000000000000000000000000000000000000000000000000L,
                        0b1000000000000000000000000000000000000000000000000000000000000000L,
                        0, 62 }, };
    }

    @DataProvider
    private static Object[][] reversedLongs() {
        return new Object[][] {
                {
                        0b1000000000000000000000000000000000000000000000000000000000000000L,
                        0b0000000000000000000000000000000000000000000000000000000000000001L },
                {
                        0b0000000000000000000000000000000000000000000000000000000000000001L,
                        0b1000000000000000000000000000000000000000000000000000000000000000L },
                {
                        0b0000000000000000000000000000000000000000000000000000000000000000L,
                        0b0000000000000000000000000000000000000000000000000000000000000000L } 
        };
    }
}
