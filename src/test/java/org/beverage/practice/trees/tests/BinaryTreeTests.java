package org.beverage.practice.trees.tests;

import java.security.SecureRandom;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.google.common.collect.Lists;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class BinaryTreeTests {

    SecureRandom random = new SecureRandom();
    
    @Test(dataProvider = "ancestorTests")
    public void commonAncestorTest(final int value1, final int value2, final int expectedResult, final TreeNode<Integer> head) {
        int ancestor = findCommonAncestor(value1, value2, head);
        
        assertThat(ancestor, equalTo(expectedResult));
    }
    
    @DataProvider
    public Object[][] ancestorTests() {
        return new Object[][] {
            { 0, 0, 0, buildFullTree(2) },
            { 1, 4, 0, buildFullTree(2) },
            { 2, 3, 1, buildFullTree(2) },
            { 2, 1, 1, buildFullTree(2) },
            { 2, 6, 0, buildFullTree(2) },
            { 5, 6, 4, buildFullTree(2) },
        };
    }
    
    public static <T> T findCommonAncestor(final T value1, final T value2, final TreeNode<T> head) {
        List<TreeNode<T>> pathToValue1 = pathTo(value1, head);
        List<TreeNode<T>> pathToValue2 = pathTo(value2, head);
        
        //  Making an assumption that searched values 
        for (int i=pathToValue1.size() - 1; i>=0; i--) {
            TreeNode<T> node = pathToValue1.get(i);
            
            if (pathToValue2.contains(node)) {
                return node.getValue();
            }
        }
        
        return null;
    }
    
    @Test(dataProvider = "pathToTestCases")
    public void pathToTests(final int value, final int pathLength, final TreeNode<Integer> head) {
        List<TreeNode<Integer>> path = pathTo(value, head);
        
        assertThat(path.size(), equalTo(pathLength));
        assertThat(path.get(0), equalTo(head));
    }
    
    @DataProvider
    public Object[][] pathToTestCases() {
        return new Object[][] {
                { 0, 1, buildFullTree(0) },
                { 0, 1, buildFullTree(1) },
                { 1, 2, buildFullTree(1) },
                { 2, 2, buildFullTree(1) },
                { 3, 3, buildFullTree(2) },
                { 6, 3, buildFullTree(2) },
        };
    }
    
    @Test(dataProvider = "testCases")
    public void buildBSTTest(final List<Integer> list) {
        //  In order traversal of tree should equal input array.
        TreeNode<Integer> bstHead = buildBSTRecursive(list);
        
        List<Integer> resultList = inOrder(bstHead);
        
        if (list.size() > 0) {
            assertThat(resultList, contains(list.toArray()));
        } else {
            assertThat(bstHead, nullValue());
        }
    }
    
    @Test(dataProvider = "staticTestCases")
    public void buildBSTTestStatic(final List<Integer> list) {
        buildBSTTest(list);
    }
    
    
    @DataProvider
    public Iterator<Object[]> testCases() {
        
        List<Object[]> testCases = new ArrayList<>();
        
        for (int i=0; i<100; i++) {
            List<Integer> array = new ArrayList<>();
            
            for (int j=0; j<random.nextInt(100); j++) {
                array.add(random.nextInt());
            }
            
            testCases.add(new Object[] { array });
        }
        
        return testCases.iterator();
    }
    
    @DataProvider
    public Object[][] staticTestCases() {
        return new Object[][] {
                { Lists.newArrayList() },
                { Lists.newArrayList(1) },
                { Lists.newArrayList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10) }
        };
    }
    
    public <T> TreeNode<T> buildBSTRecursive(final List<T> list) {
        if (list.size() == 0) {
            return null;
        }
        
        return buildBSTRecursiveInternal(list, 0, list.size() - 1);
    }
    
    public <T> TreeNode<T> buildBSTRecursiveInternal(final List<T> list, int low, int high) {
        
        int pivot = (high - low) / 2 + low;
        
        TreeNode<T> node = new TreeNode<T>(list.get(pivot));
        TreeNode<T> left = null;
        TreeNode<T> right = null;
        
        if (pivot - low > 0) {
            left = buildBSTRecursiveInternal(list, low, pivot - 1);
        }
        
        if (high - pivot > 0) {
            right = buildBSTRecursiveInternal(list, pivot + 1, high);
        }
        
        node.setLeft(left);
        node.setRight(right);
        
        return node;
    }
    
    public static <T> List<TreeNode<T>> pathTo(final T startValue, final T endValue, final TreeNode<T> tree) {
        List<TreeNode<T>> pathToStart = pathTo(startValue, tree);
        List<TreeNode<T>> pathToEnd   = pathTo(endValue, tree);
        
        if (pathToStart != null) {
            pathToEnd = pathTo(endValue, pathToStart.get(pathToStart.size() - 1));
            return pathToEnd;
        }
        
        return null;
    }
    
    public static <T> List<TreeNode<T>> pathTo(T value, final TreeNode<T> head) {
        //  Iterative post-order search.  When value is found, return the search stack state.
        //  Using addFirst/Last instead of push/pop gives us the result list starting from the
        //  head node to the target node, instead of backwards.
        Deque<TreeNode<T>> stack = new ArrayDeque<>();
        stack.addLast(head);
        
        TreeNode<T> current = head;
        TreeNode<T> previous = null;
        
        while (stack.size() > 0) {
            current = stack.getLast();

            if (current.getValue().equals(value)) {
                return new ArrayList<>(stack);
            }
            
            if (previous == null || previous.getLeft() == current || previous.getRight() == current) {
                if (current.getLeft() != null) {
                    stack.addLast(current.getLeft());
                } else if (current.getRight() != null) {
                    stack.addLast(current.getRight());
                } else {
                    stack.removeLast();
                }
            } else if (current.getLeft() == previous) {
                if (current.getRight() != null) {
                    stack.addLast(current.getRight());
                } else {
                    stack.removeLast();
                }
            } else {
                stack.removeLast();
            }
            
            previous = current;
        }
        
        return null;
    }
    
    public static <T> List<T> inOrder(final TreeNode<T> head) {
        if (head == null) {
            return null;
        }
        
        List<T> result = new ArrayList<>();
        inOrderTraversalInternal(head, result);
        return result;
    }

    private static <T> void inOrderTraversalInternal(final TreeNode<T> head, final List<T> resultList) {
        if (head.getLeft() != null) {
            inOrderTraversalInternal(head.getLeft(), resultList);
        }
        resultList.add(head.getValue());
        if (head.getRight() != null) {
            inOrderTraversalInternal(head.getRight(), resultList);
        }
    }
    
    @Data
    @Accessors(chain = true)
    @AllArgsConstructor
    public static class TreeNode<T> {
        T value;
        
        //  Store the head node for easy construction in @DataProviders.
        TreeNode<T> head;
        TreeNode<T> left;
        TreeNode<T> right;
        
        public TreeNode(T value) {
            this.value = value;
        }
        
        @Override
        public String toString() {
            return value.toString();
        }
    }
    
    public static TreeNode<Integer> buildFullTree(final int maxDepth) {
        
        TreeNode<Integer> head = new TreeNode<>(0);
        head.setHead(head);
        
        Deque<TreeNode<Integer>> nodeStack = new ArrayDeque<>();
        nodeStack.push(head);
        
        int valueCounter = 1;
        int depthCounter = 0;
        
        while (nodeStack.isEmpty() == false) {
            TreeNode<Integer> current = nodeStack.getFirst();
            
            if (depthCounter < maxDepth && current.getLeft() == null) {
                TreeNode<Integer> newLeft = new TreeNode<>(valueCounter++);
                newLeft.setHead(head);
                current.setLeft(newLeft);
                nodeStack.push(newLeft);
                depthCounter++;
            } else if (depthCounter < maxDepth && current.getRight() == null){ 
                TreeNode<Integer> newRight = new TreeNode<>(valueCounter++);
                newRight.setHead(head);
                current.setRight(newRight);
                nodeStack.push(newRight);
                depthCounter++;
            } else {
                nodeStack.pop();
                depthCounter--;
            }
        }
        
        return head;
    }
}
