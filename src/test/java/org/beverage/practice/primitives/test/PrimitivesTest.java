package org.beverage.practice.primitives.test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.text.DecimalFormat;
import java.util.Collection;

import org.beverage.practice.primitives.StringOperations;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.google.common.collect.Lists;

public class PrimitivesTest {

    private static final DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getInstance();
    
    @Test(dataProvider = "allSubStringsTestCases")
    public void allSubstringsTest(final String input, final Collection<String> expectedResults) {
        Collection<String> results = StringOperations.allSubStrings(input);

        assertThat(results, containsInAnyOrder(expectedResults.toArray()));
    }
    
    @DataProvider
    public Object[][] allSubStringsTestCases() {
        return new Object[][] {
                { "",    Lists.newArrayList() },
                { "a",   Lists.newArrayList("a") },
                { "ab",  Lists.newArrayList("ab", "ba") },
                { "abc", Lists.newArrayList("abc", "bac", "cab", "acb", "bca", "cba") },
        };
    }
    @Test(dataProvider = "atoiTestCases")
    public void atoi(final String input, final int expectedResult) {
        int actualResult = StringOperations.atoi(input);
        System.out.println(input + ", " + actualResult + ", " + expectedResult);
        assertThat(actualResult, equalTo(expectedResult));
    }
    
    @Test(dataProvider = "overflowTestCases", expectedExceptions = RuntimeException.class)
    public void atoiOverflow(final String input) {
        Integer number = StringOperations.atoi(input);
        
        //  For output purposes if this fails:
        assertThat(number, equalTo(nullValue()));
    }
    
    @Test(dataProvider = "compareToTestCases")
    public void stringCompareTo(final String left, final String right, final int expectedResult) {
        int actualResult = StringOperations.stringNumberCompareTo(left, right);
        
        assertThat(actualResult, equalTo(expectedResult));
    }
    
    @Test(dataProvider = "reverseTestCases")
    public void addBase(final String left, final String right, final int base, final String expectedResult) {
        String actualResult = StringOperations.addNumbersArbitraryBase(left, right, base);
        
        assertThat(actualResult, equalTo(expectedResult.toUpperCase()));
    }
    
    @DataProvider
    public static Object[][] reverseTestCases() {
        //  Skipping locale considerations for this problem.
        return new Object[][] {
                { "123", "234", 10, "357" },
                { "555", "666", 10, "1221" },
                { "-123", "234", 10, "111" },
                { "-555", "666", 10, "111" },
                { "555", "-666", 10, "-111" },
                { "-666", "555", 10, "-111" },
                { "-555", "-666", 10, "-1221" },
                { "1234", "111", 10, "1345" },
                { "-499", "500", 10, "1" },
                { "7ff", "800", 16, "fff" },
                { "-7ff", "800", 16, "1" },
                { "-7ff", "7ff", 16, "0" },
                { "77", "80", 8, "177" }
        };
    }
    
    @DataProvider
    public static Object[][] atoiTestCases() {
        String currencySign = decimalFormat.getDecimalFormatSymbols().getCurrencySymbol();
        char negativeSign   = decimalFormat.getDecimalFormatSymbols().getMinusSign();
        
        return new Object[][] {
                { null, 0 },
                { "", 0 },
                { "0", 0 },
                { "12345", 12345 },
                { String.format("%c%d", negativeSign, 12345), -12345 },
                { String.format("%s%d", currencySign, 12345),  12345 },
                { String.format("%c%s%d", negativeSign, currencySign, 12345), -12345 },
                //  Consider supporting this later if I come back to this problem.
                //  This case also showed that currency symbols and abbreviations should be
                //  considered at the front and back of the string.
                //{ String.format("%c%d%s", negativeSign, 12345, currencySign), -12345 },
                { decimalFormat.format(12345), 12345 },
                { decimalFormat.format(-12345), -12345 },
                { decimalFormat.format(123456789), 123456789 },
                { decimalFormat.format(12345.67), 12345 },
                { decimalFormat.format(-12345.67) ,-12345 },
                { String.format("%c%s%s", negativeSign, currencySign, decimalFormat.format(12345.67)), -12345 },
                { decimalFormat.format(Integer.MAX_VALUE), Integer.MAX_VALUE },
                { decimalFormat.format(Integer.MIN_VALUE), Integer.MIN_VALUE },
                { String.format("%s%s", currencySign, decimalFormat.format(Integer.MAX_VALUE)), Integer.MAX_VALUE },
                { String.format("%s%s%s", negativeSign, currencySign, 
                        decimalFormat.format(Integer.MAX_VALUE)).replace("647", "648"), Integer.MIN_VALUE },
        };
    }
    
    @DataProvider
    public static Object[][] overflowTestCases() {
        String currencySign = decimalFormat.getDecimalFormatSymbols().getCurrencySymbol();
        char negativeSign   = decimalFormat.getDecimalFormatSymbols().getMinusSign();
        
        return new Object[][] {
                { "12345678901" },
                { "34567890123" },
                { "12345678901234567890" },
                { decimalFormat.format(Integer.MAX_VALUE).replace("647", "648") },
                { String.format("%s%s", negativeSign, decimalFormat.format(Integer.MAX_VALUE)).replace("647", "649") },
                { String.format("%s%s", currencySign, decimalFormat.format(Integer.MAX_VALUE)).replace("647", "648") },
                { String.format("%s%s%s", negativeSign, currencySign, 
                        decimalFormat.format(Integer.MAX_VALUE)).replace("647", "649") },
        };
    }
    
    @DataProvider
    public static Object[][] compareToTestCases() {
        return new Object[][] {
                { "0", "0", 0 },
                { "-1", "1", -1 },
                { "1", "-1", 1 },
                { "1234567890", "1234567889", 1 },
                { "1234567890", "1234567890", 0 }
        };
    }
}
