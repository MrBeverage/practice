package org.beverage.practice.collections;

/**
 * Common interface for future collection objects that are not lists.  
 * Doing this now to save on refactoring costs later.  Will move {@code List}
 * members down later.
 * 
 * @author alexbeverage
 *
 * @param <T>
 */
public interface Collection<T> extends Iterable<T> {
    void add(T item);
    void addAll(T[] items);
    void addAll(Collection<T> collection);
    void clear();
    T get(int index);
    boolean remove(int index);
    boolean remove(T item);
    int size();
    T[] toArray();
    T[] toArray(T[] array);
}
