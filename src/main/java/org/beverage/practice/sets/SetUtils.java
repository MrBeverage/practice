package org.beverage.practice.sets;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Set;

public class SetUtils {
    
    public static <T> Set<Set<T>> powerSet(final Set<T> set) {

        //  Base case: powerset of empty set is the null set.
        Deque<Set<T>> queue = new ArrayDeque<Set<T>>();
        queue.push(new HashSet<T>());
        
        for (T element : set) {
            //  Add each element successively to every set in the powerset.
            //  We know we've hit every subset when we circle back around to the null set.
            do {
                //  Move the current set to the back of the queue.
                Set<T> subset = queue.pop();
                queue.addLast(subset);
                
                //  Add a copy of the set with the new element behind it.
                Set<T> newSubset = new HashSet<T>(subset);
                newSubset.add(element);
                queue.addLast(newSubset);
            } while (queue.peek().size() != 0);
        }

        Set<Set<T>> powerSet = new HashSet<Set<T>>(queue);
        return powerSet;
    }
}
