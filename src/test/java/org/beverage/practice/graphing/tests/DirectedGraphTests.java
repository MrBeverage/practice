package org.beverage.practice.graphing.tests;

import java.util.Collection;

import org.beverage.practice.graphs.DirectedGraph;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class DirectedGraphTests {

    @Test
    public void simpleGraphValues() {
        
        DirectedGraph<Integer, Integer> graph = new DirectedGraph<>();
        graph.addEdge(1, 2, 3)
             .addEdge(1, 6, 20)
             .addEdge(2, 3, 1)
             .addEdge(3, 4, 1)
             .addEdge(1, 5, 4)
             .addEdge(2, 6, 1)
             .addEdge(5, 6, 9);
             
        assertThat(graph.getSize(), equalTo(6));
        
        assertThat(graph.getEdges(1).size(), equalTo(3));
        assertThat(graph.getEdges(2).size(), equalTo(2));
        assertThat(graph.getEdges(3).size(), equalTo(1));
        assertThat(graph.getEdges(4).size(), equalTo(0));
        assertThat(graph.getEdges(5).size(), equalTo(1));
        assertThat(graph.getEdges(6).size(), equalTo(0));
    }
    
    @Test
    public void duplicateNodeValuePreservesEdges() {
        DirectedGraph<Integer, Integer> graph = new DirectedGraph<>();
        Integer value1 = new Integer(1);
        Integer value2 = new Integer(1);
        
        graph.addEdge(value1, 2, 10);
        graph.addEdge(value2, 3, 10);
        
        assertThat(graph.getSize(), equalTo(3));
        assertThat(graph.getEdges(1).size(), equalTo(2));
    }
    
    @Test(dataProvider = "simpleGraph")
    public void updateNodePreservesEdges(final DirectedGraph<Integer, Integer> graph) {
        //  Select a random node that has a connection to it, and set its value to max(graph) + 1.
        Integer maxNode = graph.getNodes().stream().max((l, r) -> Integer.compare(l, r)).get();
        
        Integer testNode = graph.getNodes().stream()
                .filter(v -> graph.getEdges(v).size() > 0)
                .findFirst().get();
                
        Integer oldNode = graph.getEdges(testNode).stream().findFirst().get();
        Integer newNode = maxNode + 1;
        
        Collection<Integer> oldEdges = graph.getEdges(oldNode);
        
        graph.updateNode(oldNode, newNode);
        
        assertThat(graph.getEdges(newNode), equalTo(oldEdges));
        assertThat(graph.getEdges(testNode).contains(newNode), equalTo(true));
    }
    
    @DataProvider
    public Object[][] simpleGraph() {
        DirectedGraph<Integer, Integer> graph = new DirectedGraph<>();
        graph.addEdge(1, 2, 3)
             .addEdge(2, 3, 1)
             .addEdge(3, 4, 1)
             .addEdge(1, 5, 4)
             .addEdge(2, 6, 1)
             .addEdge(5, 6, 9);
             
        return new Object[][] { { graph } };
    }
}
