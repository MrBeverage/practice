package org.beverage.practice.graphs;

import java.util.concurrent.atomic.AtomicInteger;

import lombok.Data;
import lombok.Getter;

/**
 * Not a particularly useful node object.  But more realistic than just storing raw values
 * in a graph for testing.
 * 
 * @author alexbeverage
 *
 * @param <ValueType>
 */
@Data
public class EdgeMapGraphNode<ValueType> {
    
    private static AtomicInteger GLOBAL_COUNTER;
    
    static {
        GLOBAL_COUNTER = new AtomicInteger();
    }
    
    //  Do I still even need or want ID fields?
    @Getter
    private Integer id;
    
    private ValueType value;

    public EdgeMapGraphNode() {
        this(null);
    }
    
    public EdgeMapGraphNode(ValueType value) {
        //  Due to type-erasure, we can't have ctors that take lists of different types, but
        //  otherwise look the same.
        this.id = GLOBAL_COUNTER.getAndIncrement();
        this.value = value;
    }
}
