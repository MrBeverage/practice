package org.beverage.practice.lists;

import org.beverage.practice.collections.Collection;

public interface List<T> extends Iterable<T>, Collection<T> {
    public void add(T value);
    public void clear();
    public boolean contains(T value);
    public T get(int index);
    public int indexOf(T value);
    public void insert(T value, int index);
    public boolean remove(T value);
    public boolean remove(int next);
    public int size();
}
