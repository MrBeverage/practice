package org.beverage.practice.lists.test;

import static org.beverage.practice.lists.ListProblems.*;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class ListProblemTests {
    
    @Test(dataProvider = "lists")
    public <T extends Comparable<T>> void listCreation(int length, ListNode<T> head) {
        
        if (head == null) {
            return;
        }
        
        ListNode<T> current = head;
        
        int i=0;
        while (current.getNext() != null) {
            assertThat(current.getValue(), equalTo(i));
            i++;
            current = current.getNext();
        }
    }
    
    @Test(dataProvider = "lists")
    public <T extends Comparable<T>> void reverse(int length, ListNode<T> head) {
        
        if (length == 0) {
            return;
        }
        
        ListNode<T> current = reverseList(head);

        for (int i = length; i > 0; i--) {
            assertThat(current.getValue(), equalTo(i-1));
            current = current.getNext();
        }
    }
    
    @Test(dataProvider = "lists")
    public <T extends Comparable<T>> void listZip(int length, ListNode<T> head) {
        
        ListNode<T> result = zipList(head);
        ListNode<T> current = result;
        
        int expectedValue;
        
        for (int i=0; i<length; i++) {
            if (i % 2 == 0) {
                expectedValue = i / 2;
            } else {
                expectedValue = length - 1 - i / 2;
            }
            
            assertThat(current.getValue(), equalTo(expectedValue));
            current = current.getNext();
        }
    }
    
    @Test(dataProvider = "mergableLists")
    public <T extends Comparable<T>> void mergeLists(
            final ListNode<T> leftList, final ListNode<T> rightList, final ListNode<T> expectedList) {
        
        ListNode<T> actualList = mergeSortedLists(leftList, rightList);
        
        assertThat(compareTo(actualList, expectedList), equalTo(0));
    }
    
    @DataProvider
    public Object[][] mergableLists() {
        return new Object[][] {
                { null, null, null },
                { null, buildList(buildIntArray(1, 20, 2)), buildList(buildIntArray(1, 20, 2)) },
                { buildList(buildIntArray(0,  20, 2)), buildList(buildIntArray(1,  21, 2)), buildList(buildIntArray(20)) },
                { buildList(buildIntArray(0,  10, 1)), buildList(buildIntArray(10, 20, 1)), buildList(buildIntArray(20)) },
                { buildList(buildIntArray(10, 20, 1)), buildList(buildIntArray(0,  10, 1)), buildList(buildIntArray(20)) }
        };
    }
    
    @Test(dataProvider = "cycleTestCases")
    public <T extends Comparable<T>> void findCycles(final ListNode<T> list, final T expectedCycleNodeValue) {
        ListNode<T> result = findCycle(list);
        
        assertThat(result.getValue(), equalTo(expectedCycleNodeValue));
    }
    
    @DataProvider
    public Object[][] cycleTestCases() {
        //  Create some useful building blocks:
        ListNode<Integer> earlyNodes = buildList(buildIntArray(5));
        ListNode<Integer> cycleNodes = buildList(buildIntArray(8, 11, 1));
        
        //  Complicated since I'm not allowed helper methods within the class itself.
        //  Maybe I should go line-by-line instead.
        ListNode<Integer> cycle1 = copyList(earlyNodes);
        ListNode<Integer> start  = getLastNode(cycle1);
        getLastNode(cycle1).setNext(copyList(cycleNodes));
        getLastNode(cycle1).setNext(start.getNext());
        
        ListNode<Integer> cycle2 = copyList(earlyNodes);
        getLastNode(cycle2).setNext(cycle2);
        
        return new Object[][] {
                { cycle1, 8 },
                { cycle2, 0 }
        };
    }
    
    public static <T extends Comparable<T>> void printList(ListNode<T> head) {
        ListNode<T> current = head;
        while (current != null) {
            System.out.print(current.getValue() + " ");
            current = current.getNext();
        }
        System.out.println();
    }
    
    @DataProvider
    private Object[][] lists() {
        return new Object[][] {
                { 10, buildList(buildIntArray(10)) },
                {  9, buildList(buildIntArray(9))  },
                {  0, null },
                {  1, new ListNode<Integer>(0, null) },
        };
    }
    
    private Integer[] buildIntArray(int maxValue) {
        return buildIntArray(0, maxValue, 1);
    }
    
    private Integer[] buildIntArray(int minValue, int maxValue, int skip) {
        Integer[] values = new Integer[(maxValue - minValue) / skip];
        
        for (int i=0; i<(maxValue - minValue) / skip; i++) {
            values[i] = minValue + skip * i;
        }
        
        return values;
    }
    
    private static <T extends Comparable<T>> ListNode<T> buildList(T[] values) {
        
        ListNode<T> head = null;
        ListNode<T> prev = null;
        
        for (int i=0; i<values.length; i++) {
            ListNode<T> node = new ListNode<T>(values[i], null);
            
            if (i != 0) {
                prev.setNext(node);
            } else {
                head = node;
            }
            
            prev = node;
        }
        
        return head;
    }
}
