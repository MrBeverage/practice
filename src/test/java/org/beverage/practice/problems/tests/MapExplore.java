package org.beverage.practice.problems.tests;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class MapExplore {

    @Test(dataProvider = "testCases")
    public void test(final int expectedIslands, final int height, final int width, final boolean[][] map) {
        int islands = countIslands(map, height, width);
        
        assertThat(islands, equalTo(expectedIslands));
    }
    
    @DataProvider
    public Object[][] testCases() {
        return new Object[][] { 
                { 0, 1, 1, new boolean[][] { { false } } },
                { 1, 1, 1, new boolean[][] { { true } } },
                { 2, 2, 2, new boolean[][] { { true, false }, { false, true } } },
                { 1, 2, 2, new boolean[][] { { true, true }, { false, false } } },
                { 1, 2, 2, new boolean[][] { { false, false }, { true, true } } },
                { 0, 2, 2, new boolean[][] { { false, false }, { false, false } } },
                { 3, 3, 3, new boolean[][] { { true, false, false }, { false, true, false }, { false, false, true } } },
                { 5, 3, 3, new boolean[][] { { true, false, true }, { false, true, false }, { true, false, true } } },
                { 3, 3, 3, new boolean[][] { { true, true, true }, { false, true, false }, { true, false, true } } },
                { 1, 3, 3, new boolean[][] { { true, true, true }, { true, true, true }, { true, true, true } } },
        };
    }
    
    public int countIslands(boolean[][] map, int height, int width) {
        
        //  Problems with using a 2d array - every row can be a different length.
        //  Is this really a problem though?  If we set the loops to check this 
        //  length and the bounds check in the exploration method as well, this would
        //  work just the same.
        
        if (map == null) {
            throw new IllegalArgumentException();
        }
        
        boolean[][] visitedMap = new boolean[height][width];
        int islands = 0;
        
        for (int y=0; y<height; y++) {
            for (int x=0; x<width; x++) {
                if (visitedMap[y][x] == false) {
                    visitedMap[y][x] = true;
                    
                    if (map[y][x] == true) {
                        //  Found an island!
                        islands++;
                        
                        exploreIsland(map, visitedMap, y-1, x);
                        exploreIsland(map, visitedMap, y+1, x);
                        exploreIsland(map, visitedMap, y, x-1);
                        exploreIsland(map, visitedMap, y, x+1);
                    }
                }
            }
        }
        
        return islands;
    }
    
    public void exploreIsland(boolean[][] map, boolean[][] visitedMap, int y, int x) {
        if (y >= 0 && y < visitedMap.length && x >= 0 && x < visitedMap[y].length && visitedMap[y][x] == false) {
            visitedMap[y][x] = true;
            
            if (map[y][x] == true) {
                exploreIsland(map, visitedMap, y-1, x);
                exploreIsland(map, visitedMap, y+1, x);
                exploreIsland(map, visitedMap, y, x-1);
                exploreIsland(map, visitedMap, y, x+1);
            }
        }
    }
}
