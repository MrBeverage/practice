package org.beverage.practice.locking;

public interface LockProvider {
    public void acquire() throws InterruptedException;
    public boolean isLocked();
    public void release();
}
