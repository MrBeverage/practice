package org.beverage.practice.primitives;

public class BitwiseOperations {

    //  5.1
    public static int parity(final long number) {
        return weight(number) % 2;
    }
    
    //  5.2
    public static long swapBits(long value, int index1, int index2) {
        if (bitAt(value, index1) == bitAt(value, index2)) {
            return value;
        }
        
        int bit1 = bitAt(value, index1);
        int bit2 = bitAt(value, index2);
        
        value = bit1 == 1 ? setBit(value, index2) : clearBit(value, index2);
        value = bit2 == 1 ? setBit(value, index1) : clearBit(value, index1);
        
        return value;
    }
    
    //  5.3
    public static long reverse(final long value) {
        long newValue = 0l;
        for (int i=0; i<64; i++) {
            newValue += ((long) bitAt(value, 64-i-1)) << i;
        }
        return newValue;
    }
    
    public static int weight(final long number) {
        long value = number;
        int weight = 0;
        for (int i=0; i<64; i++) {
            weight += value & 0x1;
            value >>= 1;
        }
        return weight;
    }
    
    public static long add(final long lh, final long rh) {
        
        long a = lh;
        long b = rh;
        long c = 0l;
        long r = 0l;
        
        for (int i=0; i<64; i++) {
            long s = (a >> i) & 0x1;
            long t = (b >> i) & 0x1;
            
            c |= (s ^ t ^ r) << i;
            r  = (s & t) | (s & r) | (t & r);
        }
        
        return c;
    }
    
    public static long divide(final long dividend, final long divisor) {
        
        if (divisor == 0) {
            throw new IllegalArgumentException("Cannot divide by zero.");
        }
        
        int sign = 1;
        
        long localDividend = dividend;
        if (localDividend < 0) {
            localDividend = twosCompliment(localDividend);
            sign *= -1;
        }
        
        long localDivisor = divisor;
        if (localDivisor < 0) {
            localDivisor = twosCompliment(localDivisor);
            sign *= -1;
        }
        
        long quotient = 0l;
        
        int iterations = significantBits(localDividend) - significantBits(localDivisor) + 1;
        
        localDivisor = alignTo(localDivisor, localDividend);
        
        for (int i=0; i<iterations; i++) {
            
            quotient <<= 1;
            
            if (localDivisor <= localDividend) {
                localDividend -= localDivisor;
                quotient |= 1;
            }
            
            localDivisor >>= 1;
        }
        
        return sign * quotient;
    }
    
    public static long alignTo(long value, long baseline) {
        int shift = significantBits(baseline) - significantBits(value);
        
        if (shift > 0) {
            return value << shift;
        } else {
            return value >> shift;
        }
    }
    
    public static int bitAt(long value, int index) {
        return (int) ((value >> index) & 0x1);
    }
    
    public static long setBit(long value, int index) {
        return value | (0x1 << index);
    }
    
    public static long clearBit(long value, int index) {
        return value & (0xffffffff - (0x1 << index));
    }
    
    public static long twosCompliment(final long value) {
        long result = value;
        result ^= -1;
        result++;
        return result;
    }
    
    public static int significantBits(long value) {
        int msbIndex = 0;
        for (int i=0; i<64; i++) {
            if (bitAt(value, i) == 1) {
                msbIndex = i;
            }
        }
        return msbIndex;
    }
}
