package org.beverage.practice.graphs;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Simplified version of Graph - does not use the node/edge containers.
 * 
 * @author alexbeverage
 *
 * @param <NodeValue>
 * @param <EdgeValue>
 */
public class DirectedGraph<NodeValue, EdgeValue> 
        implements Graph<NodeValue, EdgeValue>, Iterable<NodeValue> {

    //  Keep a node -> connections map, and a reverse lookup map.
    //  The reverse lookup map is to keep remove O(1).  Without it we would have to traverse
    //  all nodes in the primary node map looking for connections.  
    //
    //  Alternatively, we can do an O(1) lookup before returning any connection.  It would
    //  depend on the graph's application which would be more performant.
    protected final Map<NodeValue, HashMap<NodeValue, EdgeValue>> nodes;
    protected final Map<NodeValue, HashMap<NodeValue, EdgeValue>> reverseLookup;
    
    public DirectedGraph() {
        this.nodes = new HashMap<NodeValue, HashMap<NodeValue, EdgeValue>>();
        this.reverseLookup = new HashMap<NodeValue, HashMap<NodeValue, EdgeValue>>();
    }

    @Override
    public int getSize() {
        return nodes.size();
    }

    @Override
    public boolean contains(NodeValue node) {
        return nodes.containsKey(node);
    }
    
    @Override
    public Collection<NodeValue> getNodes() {
        return Collections.unmodifiableSet(nodes.keySet());
    }
    
    @Override
    public Collection<NodeValue> getEdges(NodeValue node) {
        return Collections.unmodifiableSet(nodes.get(node).keySet());
    }
    
    @Override
    public EdgeValue getEdgeValue(NodeValue left, NodeValue right) {
        if (left == null || right == null) {
            throw new IllegalArgumentException("Edge node values cannot be null.");
        }
        
        if (nodes.containsKey(left) && nodes.containsKey(right)) {
            return nodes.get(left).get(right);
        } else {
            return null;
        }
    }
    
    @Override
    public Graph<NodeValue, EdgeValue> addNode(NodeValue n) {
        if (n == null) {
            throw new IllegalArgumentException("Node value cannot be null.");
        }
        
        if (nodes.containsKey(n) == false) {
            nodes.put(n, new HashMap<>());
            reverseLookup.put(n, new HashMap<>());
        } else {
            //  The map's containsKey uses .equals, which may not compare all properties
            //  of the key object so just in case let put n with the existing connection
            //  map as an update.
            nodes.put(n, nodes.get(n));
            reverseLookup.put(n, reverseLookup.get(n));
        }
        
        return this;
    }

    @Override
    public Graph<NodeValue, EdgeValue> addNodes(Collection<NodeValue> nodes) {
        if (nodes == null) {
            throw new IllegalArgumentException("Node collection cannot be null.");
        }
        
        for (NodeValue n : nodes) {
            this.addNode(n);
        }
        
        return this;
    }

    @Override
    public Graph<NodeValue, EdgeValue> addEdge(NodeValue left, NodeValue right, EdgeValue edge) {
        
        if (left == null || right == null) {
            throw new IllegalArgumentException("Edge node values cannot be null.");
        }
        
        if (edge == null) {
            throw new IllegalArgumentException("Edge value cannot be null.");
        }
        
        if (nodes.containsKey(left) == false) {
            addNode(left);
        }
        
        if (nodes.containsKey(right) == false) {
            addNode(right);
        }
        
        //  Puts a new edge, or updates an existing edge.
        nodes.get(left).put(right, edge);
        reverseLookup.get(right).put(left, edge);
        
        return this;
    }

    @Override
    public Graph<NodeValue, EdgeValue> removeNode(NodeValue n) {
        if (n == null) {
            throw new IllegalArgumentException("Node value cannot be null.");
        }
        
        if (nodes.containsKey(n)) {
            //  Remove all connections to this node using the re
            Map<NodeValue, EdgeValue> edges = reverseLookup.get(n);
            
            for (NodeValue v : edges.keySet()) {
                nodes.get(v).remove(n);
            }
            
            //  Remove the node.
            reverseLookup.remove(n);
            nodes.remove(n);
        }
        
        return null;
    }
    
    @Override
    public Graph<NodeValue, EdgeValue> removeNodes(Collection<NodeValue> nodes) {
        if (nodes == null) {
            throw new IllegalArgumentException("Node collection cannot be null.");
        }
        
        for (NodeValue n : nodes) {
            removeNode(n);
        }
        
        return this;
    }

    @Override
    public Graph<NodeValue, EdgeValue> removeEdge(NodeValue left, NodeValue right) {
        if (left == null || right == null) {
            throw new IllegalArgumentException("Edge node values cannot be null.");
        }
        
        if (nodes.containsKey(left) && nodes.containsKey(right)) {
            nodes.get(left).remove(right);
            reverseLookup.get(right).remove(left);
        }
        
        return this;
    }

    @Override
    public Graph<NodeValue, EdgeValue> updateNode(NodeValue oldValue, NodeValue newValue) {
        if (oldValue == null || newValue == null) {
            throw new IllegalArgumentException("Previous and/or updated node values cannot be null.");
        }
        
        if (nodes.containsKey(oldValue) == false) {
            throw new IllegalArgumentException("Node " + oldValue + " is not present in the graph.");
        }
        
        if (nodes.containsKey(oldValue) == true) {
            swapMapKeys(nodes, oldValue, newValue);
            
            for (NodeValue n : reverseLookup.get(oldValue).keySet()) {
                swapMapKeys(nodes.get(n), oldValue, newValue);
            }
            
            swapMapKeys(reverseLookup, oldValue, newValue);
        }
        
        return this;
    }
    
    private <K, V> void swapMapKeys(Map<K, V> map, K oldKey, K newKey) {
        V oldValue = map.get(oldKey);
        map.remove(oldKey);
        map.put(newKey, oldValue);
    }
    
    @Override
    public Iterator<NodeValue> iterator() {
        return nodes.keySet().iterator();
    }
}
