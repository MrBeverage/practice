package org.beverage.practice.graphs;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Directed graph with edge map implementation. @see DirectedGraph for a much 
 * better and simpler implementation.
 * 
 * @author alexbeverage
 *
 * @param <T> Node value type
 * @param <E> Edge value type
 */
public class EdgeMapGraph<T, E> implements Iterable<EdgeMapGraphNode<T>> {

    private final Map<T, EdgeMapGraphNode<T>> nodeMap;
    private final Map<T, HashMap<T, EdgeMapEdge<T, E>>> edgeMap;

    public EdgeMapGraph() {
        this(null);
    }

    public EdgeMapGraph(List<EdgeMapGraphNode<T>> nodes) {
        nodeMap = new HashMap<T, EdgeMapGraphNode<T>>();
        edgeMap = new HashMap<T, HashMap<T, EdgeMapEdge<T, E>>>();
    }

    public EdgeMapGraphNode<T> getNode(T value) {
        return nodeMap.get(value);
    }

    public int size() {
        return nodeMap.size();
    }

    public Collection<EdgeMapGraphNode<T>> getNodes() {
        return nodeMap.values();
    }

    public Collection<EdgeMapEdge<T, E>> getEdges(T nodeValue) {
        return edgeMap.get(nodeValue).values();
    }

    public boolean hasEdge(T leftValue, T rightValue) {
        return edgeMap.get(leftValue).get(rightValue) != null;
    }

    public EdgeMapGraph<T, E> addNode() {
        addNode(new EdgeMapGraphNode<>(null));
        return this;
    }

    public EdgeMapGraph<T, E> addNode(T value) {
        EdgeMapGraphNode<T> newNode = new EdgeMapGraphNode<>(value);
        addNode(newNode);
        return this;
    }

    public EdgeMapGraph<T, E> addNode(EdgeMapGraphNode<T> node) {
        // This will update any existing node value if one already exists.
        // If this happens, the updated node will retain the connections of
        // the previous node.
        if (edgeMap.get(node.getValue()) == null) {
            // Initialize the left edge to right/value hashmap for this node
            // if it doesn't already exist.
            edgeMap.put(node.getValue(), new HashMap<>());
        }
        nodeMap.put(node.getValue(), node);
        return this;
    }

    public EdgeMapGraph<T, E> addEdge(T leftValue, T rightValue) {
        EdgeMapEdge<T, E> newEdge = new EdgeMapEdge<>(leftValue, rightValue);
        addEdge(newEdge);
        return this;
    }

    public EdgeMapGraph<T, E> addEdge(T leftValue, T rightValue, E edgeValue) {
        addEdge(new EdgeMapEdge<T, E>(leftValue, rightValue, edgeValue));
        return this;
    }

    public EdgeMapGraph<T, E> addEdge(EdgeMapEdge<T, E> edge) {

        if (edgeMap.get(edge.getLeft()) == null) {
            addNode(edge.getLeft());
        }

        if (edgeMap.get(edge.getRight()) == null) {
            addNode(edge.getRight());
        }

        //  This works for a directed graph. To make it undirected, put an
        //  entry in the map for the opposite direction as well.
        edgeMap.get(edge.getLeft()).put(edge.getRight(), edge);
        return this;
    }

    public EdgeMapGraph<T, E> addEdges(EdgeMapEdge<T, E>[] edges) {
        addEdges(Stream.of(edges));
        return this;
    }
    
    public EdgeMapGraph<T, E> addEdges(Stream<EdgeMapEdge<T, E>> edges) {
        edges.forEach(edge -> addEdge(edge));
        return this;
    }

    public String toStringAllNodesAndConnections() {
        StringBuilder builder = new StringBuilder();

        for (T nodeValue : nodeMap.keySet()) {

            builder.append(nodeMap.get(nodeValue).getId() + "(" + nodeValue
                    + "): ");

            for (T edgeNodeValue : edgeMap.get(nodeValue).keySet()) {
                E edgeValue = edgeMap.get(nodeValue).get(edgeNodeValue)
                        .getValue();
                builder.append(edgeNodeValue);
                if (edgeValue != null) {
                    builder.append("[" + edgeValue + "]");
                }
                builder.append(", ");
            }

            builder.append("\n");
        }

        return builder.toString();
    }

    @Override
    public Iterator<EdgeMapGraphNode<T>> iterator() {
        return nodeMap.values().iterator();
    }
}
