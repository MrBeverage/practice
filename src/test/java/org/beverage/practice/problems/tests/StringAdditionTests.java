package org.beverage.practice.problems.tests;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class StringAdditionTests {

    //  AddStrings assumes whole numbers, and ASCII digits, non-negative, and no separators:
    public long addStrings(final String s1, final String s2) {
        
        //  Put the larger of the two strings on top for easier logic later.
        final String top    = s1.length() > s2.length() ? s1 : s2;
        final String bottom = s1.equals(top) ? s2 : s1;
        
        //  Prepare the strings for addition.  There are a large number of cultural and formatting
        //  considerations to make:
        //
        //      1. Check for negative if required.  Set a flag to +/-1 and substring the
        //         negative strings appropriately.
        //      2. Use the locale-specific negative symbol.  It can change.
        //      3. Strip out separator characters if present.  These are locale-specific as well.
        //      4. Normalize the digits to ASCII if other cultures numbering systems are possible.
        //      5. These numbering systems may not be sequential within their code-pages.  Static
        //         tables converting these may be required.
        //      6. These could possibly be currency strings.
        //      7. Need to check that the numbers in the strings are in bounds, as long as long 
        //         is used.
        //
        //  Other possible approaches:
        //      1. Instead of long, return BigInteger
        //
        //  The code here will be for the simplest possible case.
        //
        
        long result = 0l;
        long tenPower = (long) Math.pow(10, top.length() - 1);
        
        for (int i=0; i<top.length(); i++) {
            //  Top digit is charAt(i), bottom digit is charAt(i - bottom.length()) > 0
            int bottomIndex = bottom.length() - top.length() + i;
            int bottomDigit = bottomIndex >= 0 ? charToDigit(bottom.charAt(bottomIndex)) : 0;
            result += tenPower * (charToDigit(top.charAt(i)) + bottomDigit);
            tenPower /= 10;
        }
        
        return result;
    }
    
    private int charToDigit(char c) {
        return c - '0';
    }
    
    @Test(dataProvider = "testCases")
    public void addStringsTest(final String s1, final String s2, final long expectedResult) {
        long result = addStrings(s1, s2);
        
        assertThat(result, equalTo(expectedResult));
    }
    
    @DataProvider
    public Object[][] testCases() {
        return new Object[][] {
                { "0", "1", 1l },
                { "1", "0", 1l },
                { "0", "0", 0l },
                { "0", "10", 10l },
                { "10", "0", 10l },
                { "9876", "54321", 64197l },
                { "2", "99999", 100001l }
        };
    }
}
