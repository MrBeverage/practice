package org.beverage.practice.datafactory;

public class SequentialIntegerFactory implements RandomObjectFactory<Integer> {
    
    int counter = 0;

    @Override
    public Integer next() {
        return counter++;
    }
}
