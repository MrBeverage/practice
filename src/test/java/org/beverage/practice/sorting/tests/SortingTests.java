package org.beverage.practice.sorting.tests;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.Iterator;

import org.beverage.practice.collections.Collection;
import org.beverage.practice.lists.ArrayList;
import org.beverage.practice.lists.test.ListTestData;
import org.beverage.practice.sorting.MergesortProvider;
import org.beverage.practice.sorting.QuicksortProvider;
import org.beverage.practice.sorting.SortProvider;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SortingTests {

    @Test(dataProvider = "integerSortingTests")
    public <T extends Comparable<? super T>> void sortArray(SortProvider<T> sortProvider, int length, T[] items) {
        
        sortProvider.sort(items);
        
        assertThat(items.length, equalTo(length));
        assertThat(isSorted(new ArrayList<>(items)), equalTo(true));
    }
    
    @Test(dataProvider = "integerSortingTests")
    public <T extends Comparable<? super T>> void sortCollection(SortProvider<T> sortProvider, int length, T[] items) {
        
        Collection<T> collection = new ArrayList<T>(items);
        
        sortProvider.sort(collection);

        assertThat(collection.size(), equalTo(length));
        assertThat(isSorted(collection), equalTo(true));
    }
    
    private <T extends Comparable<? super T>> boolean isSorted(Collection<T> values) {
        if (values.size() == 0) {
            return true;
        }
        
        T previous = values.get(0);
        
        for (int i=1; i<values.size(); i++) {
            if (previous.compareTo(values.get(i)) == 1) {
                return false;
            }
            
            previous = values.get(i);
        }
        
        return true;
    }
    
    @DataProvider
    private static Iterator<Object[]> integerSortingTests() {
        //  Schema:  sorting provider - list size - list
        //  List size is provided only for reporting purposes.
        java.util.List<Object[]> testMethodArguments = new java.util.ArrayList<Object[]>();
        
        Iterator<Object[]> listSizeTestMethodCalls = ListTestData.listSizes();
        
        SortProvider<Integer> quickSort 
            = new QuicksortProvider<>(new QuicksortProvider.Sample3MedianProvider<Integer>());
        
        SortProvider<Integer> mergeSort = new MergesortProvider<>();
        
        while (listSizeTestMethodCalls.hasNext()) {
            Integer[] items = ListTestData.buildRandomIntArray((Integer)listSizeTestMethodCalls.next()[0], 100);
            testMethodArguments.add(new Object[] { mergeSort, items.length, items });
            testMethodArguments.add(new Object[] { quickSort, items.length, items });
        }
            
        return testMethodArguments.iterator();
    }
}
