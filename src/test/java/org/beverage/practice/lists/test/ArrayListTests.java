package org.beverage.practice.lists.test;

import static org.beverage.practice.helpers.MathHelper.nextRandomPositiveInt;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.beverage.practice.lists.ArrayList;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ArrayListTests {

    @Test
    public void canExpand() {
        ArrayList<Integer> list = new ArrayList<>();
        
        int baseCapacity = list.getCapacity();
        
        for (int i=0; i<baseCapacity + 1; i++) {
            list.add(i);
        }
        
        assertThat(list.getCapacity(), greaterThan(baseCapacity));
    }
    
    @Test
    public void canIterate() {
        ArrayList<Integer> list = new ArrayList<>();
        
        for (int i=0; i<100; i++) {
            list.add(i);
        }
        
        for (int i=0; i<100; i++) {
            assertThat(list.get(i), equalTo(i));
        }
    }
    
    @Test
    public void canClear() {
        ArrayList<Integer> list = (ArrayList<Integer>) ListTestData.buildArrayList(
                new ArrayList<Integer>(), ListTestData.buildIntArray(100));
        
        list.clear();
        assertThat(list.size(), equalTo(0));
    }
    
    @Test(dataProvider = "listSizesAndLocations")
    public void canInsert(int listSize, int operationLocation) {
        ArrayList<Integer> list = (ArrayList<Integer>) ListTestData.buildArrayList(
                new ArrayList<Integer>(), ListTestData.buildIntArray(listSize));
        
        int previousSize = list.size();
        
        list.insert(-1, operationLocation);
        
        assertThat(list.size(), equalTo(previousSize + 1));
        assertThat(list.get(operationLocation), equalTo(-1));
    }
    
    @Test(dataProvider = "listSizesAndLocations")
    public void canRemove(int listSize, int operationLocation) {
        ArrayList<Integer> list = (ArrayList<Integer>) ListTestData.buildArrayList(
                new ArrayList<Integer>(), ListTestData.buildIntArray(listSize));
        
        int previousSize = list.size();
        Integer expectedValue = 
                operationLocation < list.size() - 1 ? 
                        list.get(operationLocation + 1) : 
                        null;
        
        boolean result = list.remove(operationLocation);
        
        assertThat(result, equalTo(true));
        assertThat(list.size(), equalTo(previousSize - 1));

        if (operationLocation == listSize - 1){
            assertThat(list.size(), equalTo(operationLocation));
        } else if (list.size() != 0) {
            assertThat(list.get(operationLocation), equalTo(expectedValue));
        } else {
            assertThat(list.get(operationLocation), equalTo(null));
        }
    }
    
    @DataProvider
    private java.util.Iterator<Object[]> listSizesAndLocations() {
        java.util.ArrayList<Object[]> testMethodArguments = new java.util.ArrayList<>();
        
        //  Schema: listSize, operationAt (if applicable)
        //  Static test cases:
        testMethodArguments.add(new Object[] { 1, 0 });
        testMethodArguments.add(new Object[] { 10, 0 });
        testMethodArguments.add(new Object[] { 10, 9 });
        testMethodArguments.add(new Object[] { 10, nextRandomPositiveInt(10) });
        testMethodArguments.add(new Object[] { 10, nextRandomPositiveInt(10) });
        testMethodArguments.add(new Object[] { 10, nextRandomPositiveInt(10) });
        testMethodArguments.add(new Object[] { 10, nextRandomPositiveInt(10) });
        testMethodArguments.add(new Object[] { 11, 0 });
        testMethodArguments.add(new Object[] { 11, nextRandomPositiveInt(11) });
        testMethodArguments.add(new Object[] { 11, nextRandomPositiveInt(11) });
        testMethodArguments.add(new Object[] { 11, nextRandomPositiveInt(11) });
        testMethodArguments.add(new Object[] { 11, nextRandomPositiveInt(11) });
        testMethodArguments.add(new Object[] { 11, 10 });
        
        //  Some small, random lists:
        for (int i=0; i<25; i++) {
            int randomSize = nextRandomPositiveInt(100);
            testMethodArguments.add(new Object[] { randomSize, 0 });
            testMethodArguments.add(new Object[] { randomSize, nextRandomPositiveInt(randomSize) });
            testMethodArguments.add(new Object[] { randomSize, randomSize - 1 });
        }
        
        //  A few large, random lists:
//        for (int i=0; i<3; i++) {
//            int randomSize = nextPositiveInt(10) * 100000;
//            testMethodArguments.add(new Object[] { randomSize, 0 });
//            testMethodArguments.add(new Object[] { randomSize, nextPositiveInt(randomSize) });
//            testMethodArguments.add(new Object[] { randomSize, randomSize - 1 });
//        }
        
        return testMethodArguments.iterator();
    }
}
