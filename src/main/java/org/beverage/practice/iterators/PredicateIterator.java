package org.beverage.practice.iterators;

import java.util.Iterator;
import java.util.function.Predicate;

import org.beverage.practice.collections.Collection;

public class PredicateIterator<T> implements Iterator<T> {

    private final Predicate<T> predicate;
    private final Collection<T> collection;
    
    private boolean hasNext = false;
    private int lastReturned = -1;
    private int cursor = 0;
    
    public PredicateIterator(final Collection<T> collection, final Predicate<T> predicate) {
        if (collection == null) {
            throw new IllegalArgumentException("collection must not be null");
        }
        if (predicate == null) {
            throw new IllegalArgumentException("predicate must not be null");
        }
        
        this.collection = collection;
        this.predicate = predicate;
    }
    
    @Override
    public boolean hasNext() {
        while (cursor < collection.size() && predicate.test(collection.get(cursor)) == false) {
            cursor++;
        }
        
        if (cursor != collection.size()) {
            hasNext =  true;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public T next() {
        if (hasNext == true) {
            hasNext = false;
        } else if (hasNext() == false) {
            return null;
        } 
        
        lastReturned = cursor;
        return collection.get(cursor++);
    }

    @Override
    public void remove() {
        if (lastReturned > -1) {
            collection.remove(lastReturned);
            cursor = lastReturned;
        } else if (lastReturned == cursor) {
            throw new IllegalStateException("remove() can only be called once per call to next()");
        } else {
            throw new IllegalStateException("remove() cannot be called without first returning an item from next()");
        }
    }
}
