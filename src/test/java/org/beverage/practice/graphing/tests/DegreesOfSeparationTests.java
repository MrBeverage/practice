package org.beverage.practice.graphing.tests;

import static org.beverage.practice.graphing.tests.GraphTestData.buildSequentialNodeValueGraph;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.TreeSet;

import org.beverage.practice.graphs.EdgeMapEdge;
import org.beverage.practice.graphs.EdgeMapGraph;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DegreesOfSeparationTests {

    @DataProvider
    public Object[][] testCases() {
        //  Name, graph, target node, expected depth.
        return new Object[][] {
                { "Single node", buildSequentialNodeValueGraph(1, 0), 0, 0, 0 },
                { "Single loop", buildSequentialNodeValueGraph(2, 1), 0, 1, 1 },
                { "No solution", new EdgeMapGraph<Integer, Integer>().addEdge(0, 1).addEdge(2, 3), 0, 2, Integer.MAX_VALUE },
                { "Distance 2", new EdgeMapGraph<Integer, Integer>().addEdge(0, 1).addEdge(1, 2), 0, 2, 2 },
                { "Strings", new EdgeMapGraph<String, String>().addEdge("a", "b").addEdge("b", "c"), "a", "c", 2 },
                { "Far edge shallow", new EdgeMapGraph<String, String>()
                    .addEdge("a", "b").addEdge("b", "c").addEdge("c", "d")
                    .addEdge("a", "d"), "a", "d", 1 },
                { "Far edge deep", new EdgeMapGraph<String, String>()
                    .addEdge("a", "b").addEdge("b", "c").addEdge("c", "d").addEdge("d", "e")
                    .addEdge("a", "f").addEdge("f", "e"), "a", "e", 2 },
        };
    }
    
    @Test(dataProvider = "testCases")
    public <T, E> void degreesOfSeparationDfs(
            final String name, final EdgeMapGraph<T, E> graph, 
            final T startNodeValue, final T targetNodeValue, final int expectedDepth) {

        //  Treeset is nice for large, sparse graphs where the quality of visited nodes could
        //  be quite high.  Kind of overkill here though.
        TreeSet<T> visitedNodes = new TreeSet<T>();
        Deque<T> remainingNodes = new ArrayDeque<T>();
        
        remainingNodes.push(startNodeValue);
        visitedNodes.add(startNodeValue);
        int bestDepth = Integer.MAX_VALUE;
        
        while (remainingNodes.size() > 0) {
            
            if (remainingNodes.size() > bestDepth) {
                //  We've exceeded the depth of our current best solution.  Pop upwards.
                remainingNodes.pop();
                continue;
            }
            
            T currentNode = remainingNodes.getFirst();
            
            if (remainingNodes.getFirst().equals(targetNodeValue)) {
                bestDepth = remainingNodes.size() - 1;
            }
            
            for (EdgeMapEdge<T, E> edge : graph.getEdges(currentNode)) {
                
                T rightValue = edge.getRight();
                
                //  Push any child value that hasn't already been visited in this subtree on the stack.
                //  Mark it as visited so we don't get into a loop.
                if (visitedNodes.contains(rightValue) == false) {
                    remainingNodes.push(rightValue);
                    visitedNodes.add(rightValue);
                    break;
                }
            }
            
            if (currentNode.equals(remainingNodes.getFirst())) {
                //  No valid children were found.  Remove the children from the visited list
                //  and pop up the stack.  Leave the current node in the visited list so we
                //  don't head back down that tree - they can be revisited later in other sub-trees.
                for (EdgeMapEdge<T, E> edge : graph.getEdges(currentNode)) {
                    visitedNodes.remove(edge.getRight());
                }
                
                remainingNodes.pop();
            }
        }

        assertThat(bestDepth, equalTo(expectedDepth));
    }
    
    @Test(dataProvider = "testCases")
    public <T, E> void degreesOfSeparationBfs(
            final String name, final EdgeMapGraph<T, E> graph, 
            final T startNodeValue, final T targetNodeValue, final int expectedDepth) {
        
        TreeSet<T> visitedNodes = new TreeSet<>();
        Deque<T> remainingNodes = new ArrayDeque<>();
        
        boolean found = false;
        int distance = -1;
        remainingNodes.push(startNodeValue);
        
        OuterLoop:
        while (remainingNodes.size() > 0) {

            distance++;
            
            int nodesAtLevel = remainingNodes.size();
            
            for (int i=0; i<nodesAtLevel; i++) {
                T currentNode = remainingNodes.removeFirst();

                if (currentNode.equals(targetNodeValue)) {
                    found = true;
                    break OuterLoop;
                }
                for (EdgeMapEdge<T, E> edge : graph.getEdges(currentNode)) {
                    if (visitedNodes.contains(edge.getRight()) == false) {
                        remainingNodes.addLast(edge.getRight());
                    }
                }
                
                visitedNodes.add(currentNode);
            }
        }
        
        if (expectedDepth == Integer.MAX_VALUE) {
            assertThat(found, equalTo(false));
        } else {
            assertThat(distance, equalTo(expectedDepth));
        }
    }
}
