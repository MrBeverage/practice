package org.beverage.practice.trees.tests;

import java.util.ArrayDeque;
import java.util.Deque;

import org.beverage.practice.trees.tests.TraversalOrderTests.TreeNode;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class BalanceTests {

    @Test(dataProvider = "trees")
    public void treeBalance(final boolean expectedResult, final TreeNode head) {
        boolean result = isBalanced(head);
        
        assertThat(result, equalTo(expectedResult));
    }
    

    @DataProvider
    public Object[][] trees() {
        TreeNode lImbalance = new TreeNode(11111111).setLeft(new TreeNode(33333333));
        TreeNode rImbalance = new TreeNode(11111111).setRight(new TreeNode(44444444));
        
        return new Object[][] { 
            { true,  TraversalOrderTests.buildFullTree(0) },
            { false, TraversalOrderTests.buildFullTree(0).setLeft(lImbalance) },
            { false, TraversalOrderTests.buildFullTree(0).setLeft(rImbalance) },
            { false, TraversalOrderTests.buildFullTree(0).setRight(lImbalance) },
            { false, TraversalOrderTests.buildFullTree(0).setRight(rImbalance) },
            { true,  TraversalOrderTests.buildFullTree(1) },
            { true,  TraversalOrderTests.buildFullTree(1).getLeft().setLeft(new TreeNode(100)) },
            { true,  TraversalOrderTests.buildFullTree(1).getLeft().setRight(new TreeNode(100)) },
            { true,  TraversalOrderTests.buildFullTree(1).getRight().setLeft(new TreeNode(100)) },
            { true,  TraversalOrderTests.buildFullTree(1).getRight().setRight(new TreeNode(100)) },
            { true,  TraversalOrderTests.buildFullTree(2) },
            { false, TraversalOrderTests.buildFullTree(1).getLeft().setLeft(new TreeNode(100).setLeft(new TreeNode(200))) },
        };
    };
    
    public boolean isBalanced(final TreeNode head) {
        
        //  I realize now that any traversal method could work for this, however post-order 
        //  provides the advantage that the working stack's size is always the current 
        //  height.  There are other ways to keep track of that though.
        
        TreeNode current;
        TreeNode previous = null;
        
        int maxDepth = 1;
        int offset = 0;
        boolean boundsAreSet = false;
        
        Deque<TreeNode> stack = new ArrayDeque<TreeNode>();
        stack.push(head);
        
        //  This is a post-order traversal.  When we hit leaf nodes we need to check for
        //  balance, or work on setting maxDepth/offset if they are not already set.
        while (stack.size() > 0) {
            current = stack.getFirst();
            
            //  If either connection is terminal, the node must be within the maxDepth/offset range:
            if (boundsAreSet && (current.getLeft() == null || current.getRight() == null)) {
                if (isWithin(stack.size(), maxDepth, offset) == false) {
                    return false;
                }
            }
            
            if (previous == null || previous.getLeft() == current || previous.getRight() == current) {
                if (current.getLeft() != null) {
                    stack.push(current.getLeft());
                } else {
                    //  Once the left-most path is completed, set maxDepth and offset:
                    if (boundsAreSet == false) {
                        maxDepth = stack.size();
                        offset = current.getRight() == null ? -1 : 1;
                        boundsAreSet = true;
                    }
                    
                    if (current.getRight() != null) {
                        stack.push(current.getRight());
                    } else {
                        stack.pop();
                    }
                }
            } else if (current.getLeft() == previous) {
                //  Here we are backtracking and taking the previous right, if possible.
                if (current.getRight() != null) {
                    stack.push(current.getRight());
                } else {
                    stack.pop();
                }
            } else {
                //  Here we are walking back multiple levels in the tree.
                stack.pop();
            }
            
            previous = current;
        }
        
        return true;
    }
    
    private static boolean isWithin(int value, int target, int offset) {
        return target - value == 0 || target - value + offset == 0;
    }
}
