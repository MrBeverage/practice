package org.beverage.practice.util;

import java.lang.ref.WeakReference;

public class GCUtils {
    
    public static void waitForGC() throws InterruptedException {
        Object wrObject = new Object();
        WeakReference<Object> wrWait = new WeakReference<>(wrObject);
        wrObject = null;
        System.gc();
        
        while (wrWait.get() != null) {
            Thread.sleep(500);
        }
    }
}
