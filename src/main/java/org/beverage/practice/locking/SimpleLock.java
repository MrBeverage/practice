package org.beverage.practice.locking;

import java.util.concurrent.ConcurrentLinkedDeque;

public class SimpleLock implements LockProvider {

    private boolean isLocked = false;
    private Thread lockingThread = null;
    private final ConcurrentLinkedDeque<Semaphore> waitingThreads;
    
    public SimpleLock() {
        waitingThreads = new ConcurrentLinkedDeque<>();
    }
    
    @Override
    public boolean isLocked() {
        return isLocked;
    }
    
    @Override
    public void acquire() throws InterruptedException {
        boolean blocked = true;
        Semaphore semaphore = new Semaphore();
        waitingThreads.push(semaphore);
        
        while (blocked) {
            //  While the resource is locked, or while this thread is not next in 
            //  line, keep waiting.  The peek/remove combo must be atomic here.
            synchronized (this) {
                blocked = isLocked || waitingThreads.peekFirst().equals(semaphore) == false;
                
                if (!blocked) {
                    isLocked = true;
                    waitingThreads.removeFirst();
                    lockingThread = Thread.currentThread();
                    return;
                }
            }
            
            try {
                semaphore.doWait();
            } catch (InterruptedException ie) {
                waitingThreads.remove(semaphore);
                throw ie;
            }
        }
    }

    @Override
    public void release() {
        if (lockingThread != Thread.currentThread()) {
            throw new IllegalMonitorStateException("Calling thread does not possess this lock");
        }
        
        isLocked = false;
        lockingThread = null;
        if (waitingThreads.size() > 0) {
            waitingThreads.peekFirst().doNotify();
        }
    }
}
