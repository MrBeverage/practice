package org.beverage.practice.sorting;

import java.security.SecureRandom;
import java.util.Arrays;

import org.beverage.practice.collections.Collection;

public class QuicksortProvider<T extends Comparable<? super T>> implements SortProvider<T> {

    private final PivotSelector<T> pivotSelector;
    
    public QuicksortProvider(PivotSelector<T> pivotSelector) {
        this.pivotSelector = pivotSelector;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public void sort(Collection<T> collection) {
        if (collection == null || collection.size() <= 1) {
            return;
        }
        
        Object[] items = collection.toArray();
        recursiveInternal(items, 0, collection.size() - 1);
        
        collection.clear();
        
        for (Object item : items) {
            collection.add((T) item);
        }
    }

    @Override
    public void sort(T[] items) {
        if (items == null || items.length <= 1) {
            return;
        }
        
        recursiveInternal((Object[]) items, 0, items.length - 1);
    }

    protected Object[] recursiveInternal(Object[] items, int lowerBound, int upperBound) {
        
        if (upperBound - lowerBound == 0) {
            //  1-case: sorted.
            return items;
        } else if (upperBound - lowerBound == 1) {
            //  2-case: just swap 'em if needed.
            if (SortHelper.objectCompareTo(items[lowerBound], items[upperBound]) == 1) {
                SortHelper.swapItems(items, lowerBound, upperBound);
            }
            return items;
        } else {
            //  >2-case: Pivot and recurse.
            int pivot = pivotSelector.selectPivot(items, lowerBound, upperBound);
            Object pivotValue = items[pivot];
            
            SortHelper.swapItems(items, pivot, upperBound); 
            int swapIndex = lowerBound;
            
            for (int i=lowerBound; i<upperBound; i++) {
                if (SortHelper.objectCompareTo(items[i], pivotValue) < 0) {
                    SortHelper.swapItems(items, i, swapIndex++);
                }
            }
            
            SortHelper.swapItems(items, swapIndex, upperBound);
            
            //  Recurse down to 1..2 sized bits:
            if (lowerBound <= swapIndex-1) {
                recursiveInternal(items, lowerBound, swapIndex-1); 
            }
            
            if (swapIndex+1 <= upperBound) {
                recursiveInternal(items, swapIndex+1, upperBound);
            }
            
            return items;
        }
    }
    
    public void swapItems(T[] items, int pos1, int pos2) {
        T temp = items[pos1];
        items[pos1] = items[pos2];
        items[pos2] = temp;
    }
    
    public static class Sample3MedianProvider<T extends Comparable<? super T>> implements PivotSelector<T> {

        final SecureRandom random = new SecureRandom();
        
        @Override
        public int selectPivot(Object[] items, int lowerBound, int upperBound) {
            if (items == null || items.length == 0) {
                throw new IllegalArgumentException("Array is null or empty.");
            }
            
            int bound = upperBound - lowerBound;
            int[] indices = new int[3];
            
            if (bound > 3) {
                indices[0] = lowerBound + random.nextInt(bound);
                indices[1] = lowerBound + getRandomIndexExcluding(bound, indices[0], -1);
                indices[2] = lowerBound + getRandomIndexExcluding(bound, indices[0], indices[1]);
            } else if (bound == 3){
                indices[0] = lowerBound;
                indices[1] = lowerBound + 1;
                indices[2] = lowerBound + 2;
            } else if (bound < 3) {
                //  Size 1, 2 just take the first value as median.
                return lowerBound;
            } 
            
            int sum = 0;
            
            for (int i : indices) {
                sum = 0;
                
                for (int j : indices) {
                    if (j != i) {
                        sum += SortHelper.objectCompareTo(items[i], items[j]);
                    }
                }
                
                if (sum == 0) {
                    return i;
                }
            }
            
            //  If we got here, that means either the min or max value has a duplicate.
            //  Find the duplicate and return its index. 
            for (int i=0; i<3; i++) {
                for (int j=i+1; j<3; j++) {
                    if (SortHelper.objectCompareTo(items[indices[i]], items[indices[j]])== 0) {
                    //if (items[indices[i]].compareTo(items[indices[j]]) == 0) {
                        return indices[i];
                    }
                }
            }
            //  Shouldn't get here.
            throw new IllegalStateException("Somehow we don't have a pivot - Indices: " + 
                    Arrays.toString(indices) + ", Array: " + Arrays.toString(items));
        }
        
        /**
         * Returns a random index into a set of items that is guaranteed to not be up to two provided
         * indices.
         * @param bound maximum possible random number to generate
         * @param excludeIndex1 first index to exclude - required
         * @param excludeIndex2 second index to exclude - optional, provide -1 to have it ignored.
         * @return random index that is not either excludeIndex1 or excludeIndex2
         */
        public int getRandomIndexExcluding(int bound, int excludeIndex1, int excludeIndex2) {
            
            //  Input validation needed.
            
            int randomIndex = random.nextInt(bound - 2);
            
            //  Shifting the random index by up to +2 on collisions effectively maps 0..bound-2 to
            //  0..bound with the exclusions.  This should not skew the dataset.
            if (randomIndex == excludeIndex1 || randomIndex == excludeIndex2) {
                
                randomIndex++;
                
                if (randomIndex == excludeIndex1 || randomIndex == excludeIndex2) {
                    randomIndex++;
                }
            }

            return randomIndex;
        }
    }
}
