package org.beverage.practice.lists;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.RandomAccess;

import org.beverage.practice.collections.Collection;

public class ArrayList<T> extends AbstractList<T> implements List<T>, RandomAccess  {

    private static final int MIN_CAPACITY = 10;
    private static final int MAX_CAPACITY = 0x7ffffff7;     //  Stolen from actual ArrayList implementation.
    
    private Object[] elements;
    
    public ArrayList() {
        this(MIN_CAPACITY);
    }
    
    public ArrayList(int size) {
        this.elements = new Object[size];
    }
    
    public ArrayList(T[] items) {
        this(items.length);
        this.addAll(items);
    }
    
    public int getCapacity() {
        return elements.length;
    }
    
    /**
     * Raises the capacity of the underlying array to at least a minimum specified value, 
     * or exactly a specified value, or the maximum possible size if the requested or usual
     * growth step is not possible.
     * @param minCapacity requested minimum capacity, or exact capacity if strict is true
     * @param strict whether or not the minCapacity parameter is to be used exactly as given
     */
    protected void checkCapacity(int minCapacity, boolean strict) {
        //  Implementation details stolen from proper ArrayList:
        if (minCapacity > elements.length && minCapacity > MIN_CAPACITY) {
            incrementRevisionNumber();
            
            int newCapacity = 0;
            if (strict == false) {
                newCapacity = elements.length + (elements.length >> 1);
            } else {
                newCapacity = minCapacity;
            }
            
            //  Neat overflow safeguards:
            if (newCapacity - minCapacity < 0) {
                newCapacity = minCapacity;
            }
            if (newCapacity - Integer.MAX_VALUE > 0) {
                newCapacity = MAX_CAPACITY;
            }
            
            elements = Arrays.copyOf(elements, newCapacity);
        }
    }
    
    public void resize(int newSize) {
        if (newSize <= elements.length) {
            throw new IllegalArgumentException("New size is same as or less than current size: " + elements.length);
        }
        
        //  I don't think I need this here.  The actual contents don't change, so anything
        //  iterating through the collection should still work fine...
        checkCapacity(newSize, true);
    }
    
    public void trimToSize() {
        incrementRevisionNumber();
        elements = Arrays.copyOf(elements, size());
    }
    
    @Override
    public void add(T value) {
        checkCapacity(size() + 1, false);
        elements[size()] = value;
        incrementSize();
    }
    
    @Override
    public void addAll(T[] items) {
        int minCapacity = elements.length;
        if (elements.length - size() - items.length < 0) {
            minCapacity = size() + items.length;
        }
        
        //  A collection already cannot be greater than MAX_CAPACITY which is less than 0x7fffffff.
        //  It is therefore safe to do this check for an overflow.
        if (minCapacity < 0) {
            //  We've overflowed.  Cannot support a list this size.
            throw new IndexOutOfBoundsException("Required list size exceeds maximum allowable: " + MAX_CAPACITY);
        }
        
        checkCapacity(minCapacity, false);
        
        System.arraycopy(items, size(), elements, size(), items.length);
        setSize(size() + items.length);
    }
    
    @Override
    public void addAll(Collection<T> list) {
        addAll(list.toArray());
    }
    
    @Override
    public void clear() {
        incrementRevisionNumber();
        elements = new Object[elements.length];
        setSize(0);
    }
    
    @Override
    public boolean contains(T value) {
        for (Object element : elements) {
            if (element.equals(value)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void insert(T value, int index) {
        checkCapacity(size() + 1, false);
        System.arraycopy(elements, index, elements, index+1, size()-index);
        elements[index] = value;
        incrementSize();
    }

    @Override
    public boolean remove(T value) {
        int index = indexOf(value);
        return index == -1 ? false : remove(index);
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean remove(int index) {
        if (index > size()) {
            throw new IllegalArgumentException("Index is greater than the List's size.");
        }
        Object temp = null;
        
        incrementRevisionNumber();
        if (size() == 1) {
            temp = elements[index];
            elements[0] = null;
        } else {
            temp = elements[index];
            System.arraycopy(elements, index+1, elements, index, size()-index-1);
        }
        
        decrementSize();
        return (T) temp == null ? false : true;
    }

    @SuppressWarnings("unchecked")
    @Override
    public T[] toArray() {
        T[] copy = (T[]) Arrays.copyOf(elements, size());
        return copy;
    }
    
    @Override
    public T[] toArray(T[] array) {
        if (array == null) {
            throw new IllegalArgumentException("Destination array is null.");
        }
        
        T[] newArray = Arrays.copyOf(array, size());
        for (int i=0; i<size(); i++) {
            newArray[i] = get(i);
        }
        return newArray;
    }

    @SuppressWarnings("unchecked")
    @Override
    public T get(int index) {
        if (index >= size()) {
            throw new IndexOutOfBoundsException();
        }
        return (T) elements[index];
    }

    @Override
    protected AbstractList.BasicListNode<T> getNode(int index) {
        return null;
    }
    
    @Override
    public Iterator<T> iterator() {
        return new ArrayListIterator();
    }
    
    public class ArrayListIterator implements Iterator<T> {

        private int cursor = 0;
        private int lastReturned = -1;
        private int expectedRevision;
        
        public ArrayListIterator() {
            this.cursor = 0;
            this.lastReturned = -1;
            this.expectedRevision = ArrayList.this.revisionNumber;
        }
        
        @Override
        public boolean hasNext() {
            return cursor != ArrayList.this.size();
        }

        @SuppressWarnings("unchecked")
        @Override
        public T next() {
            ArrayList.this.checkForComodification(expectedRevision);
            if (cursor > elements.length) {
                throw new ConcurrentModificationException("ArrayList has shrunk mid-iteration.");
            }
            ++cursor;
            return (T) ArrayList.this.elements[++lastReturned];
        }
        
        @Override
        public void remove() {
            if (lastReturned < 0) {
                throw new IllegalStateException("Cannot call remove() on an unused iterator.  Call next() first.");
            }
            
            try {
                ArrayList.this.checkForComodification(expectedRevision);
                ArrayList.this.remove(lastReturned);
                cursor--;
                lastReturned--;
            } catch (IndexOutOfBoundsException ie) {
                throw new ConcurrentModificationException("ArrayList size has changed mid-iteration.", ie);
            }
        }
    }
}
