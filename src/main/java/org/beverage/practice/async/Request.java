package org.beverage.practice.async;

public interface Request<T> {

    public T execute(T request);
    public void handleResponse(T response);
    public void handleError();
}
