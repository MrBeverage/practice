package org.beverage.practice.async;

import org.beverage.practice.primitives.StringOperations;

/***
 * Simple non-generic implementation of Request<> for problem 18.3
 */
public abstract class ReverseStringRequest implements Request<String> {
    
    private final long sleepTime;
    
    public ReverseStringRequest(long sleepTime) {
        this.sleepTime = sleepTime;
    }
    
    public String execute(String request) {
        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            
        }
        return StringOperations.reverse(request);
    }
    
    public void handleError() {
        
    }
    
    public abstract void handleResponse(String response);
}
