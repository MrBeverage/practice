package org.beverage.practice.sorting;

public class SortHelper {
    //  This is safe because we cannot construct this class with T extends Comparable<? super T> 
    //  constraint  satisfied meaning we can freely cast back and forth between Object.
    //  I hate type erasure.
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static int objectCompareTo(Object left, Object right) {
        Comparable leftComparable = (Comparable) left;
        Comparable rightComparable = (Comparable) right;
        
        return leftComparable.compareTo(rightComparable);
    }
    
    public static void swapItems(Object[] items, int left, int right) {
        Object temp = items[left];
        items[left] = items[right];
        items[right] = temp;
    }
}
