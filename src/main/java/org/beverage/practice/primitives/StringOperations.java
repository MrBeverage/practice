package org.beverage.practice.primitives;

import java.text.DecimalFormat;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Deque;

public class StringOperations {

    private static final DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getInstance();

    //  5.6
    /**
     * String to integer method that can handle separators, decimals, negatives, and currency, and 
     * all in the same number string.
     * @param number
     * @return
     */
    public static int atoi(final String number) {
        
        if (number == null || number.length() == 0) {
            return 0;
        }
        
        String original = number;
        int newNumber = 0;
        
        String currencySign = decimalFormat.getDecimalFormatSymbols().getCurrencySymbol();
        char decimal        = decimalFormat.getDecimalFormatSymbols().getDecimalSeparator();
        char separator      = decimalFormat.getDecimalFormatSymbols().getGroupingSeparator();
        char negativeSign   = decimalFormat.getDecimalFormatSymbols().getMinusSign();
        
        int numberChars = original.length();
        int sign = 1;
        
        //  Replace any separator characters.
        if (original.contains(String.valueOf(separator))) {
            original = original.replace(String.valueOf(separator), "");
            numberChars = original.length();
        }
        
        //  Remove any decimal portion.
        if (original.contains(String.valueOf(decimal))) {
            original = original.substring(0, original.indexOf(decimal));
            numberChars = original.length();
        }
        
        //  Assumption: a negative sign is always first.
        if (original.charAt(0) == negativeSign) {
            sign = -1;
            numberChars--;
        }
        
        //  Assumption: a currency symbol is always second.
        if (original.contains(currencySign)) {
            numberChars -= currencySign.length();
        }
        
        if (numberChars > 10) {
            throw new RuntimeException("Number is too large to be stored as an integer.");
        }
        
        int currentDigit;
        
        for (int i=0; i != numberChars; i++) {
            currentDigit = original.charAt(original.length() - i - 1) - '0';
            
            if (currentDigit < 0 || currentDigit > 9) {
                throw new IllegalArgumentException("Non-numeric character present.");
            }
            
            if (i == 9 && currentDigit > 2) {
                //  This can be different depending on JVM implementations.  Should look directly
                //  At the final digit and order of magnitude of min/max int.
                throw new RuntimeException("Number is too large to be stored as an integer.");
            } else if (i == 9) {
                newNumber += sign * currentDigit * (int) Math.pow(10, i);
                if (sign == 1 && newNumber < 0) {
                    throw new RuntimeException("Number is too large to be stored as an integer.");
                } else if (sign == -1 && newNumber > 0) {
                    throw new RuntimeException("Number is too large to be stored as an integer.");
                }
            } else {
                newNumber += sign * currentDigit * Math.pow(10.0, i);
            }
        }
        
        return newNumber;
    }

    public static String addNumbersArbitraryBase(final String left, final String right, final int base) {
        
        StringBuilder result = new StringBuilder();
        String negativeSign   = String.valueOf(StringOperations.decimalFormat.getDecimalFormatSymbols().getMinusSign());
        
        String top;
        String bottom;
        
        if (stringNumberCompareTo(left, right) > 1) {
            top = left;
            bottom = right;
        } else {
            top = right;
            bottom = left;
        }
        
        int topSign = top.charAt(0) == negativeSign.charAt(0) ? -1 : 1;
        if (topSign == -1) {
            top = top.replace(negativeSign, "");
        }
        
        int bottomSign = bottom.charAt(0) == negativeSign.charAt(0) ? -1 : 1;
        if (bottomSign == -1) {
            bottom = bottom.replace(negativeSign, "");
        }
        
        //  Make sure the greater magnitude is always on top.
        if (stringNumberCompareTo(top, bottom) < 0) {
            String tempStr = top;
            top = bottom;
            bottom = tempStr;
            
            int tempSign = topSign;
            topSign = bottomSign;
            bottomSign = tempSign;
        }
        
        int carry = 0;
        int borrow = 0;
        
        int current;
        int topDigit;
        int bottomDigit;
        
        for (int i=0; i<top.length(); i++) {
            
            topDigit = topSign * (charToDigit(top.charAt(top.length()-i-1)));
            
            //  Reduce the top digit for a borrow if one was taken.  Borrow from
            //  the next digit if necessary.
            if (borrow != 0) {
                if (topDigit != 0) {
                    topDigit -= topSign;
                    borrow = 0;
                } else {
                    topDigit += topSign * base;
                    topDigit -= topSign;
                }
            }
            
            //  Get a bottom digit if available.
            if (i < bottom.length()) {
                bottomDigit = bottomSign * (charToDigit(bottom.charAt(bottom.length()-i-1)));
            } else {
                bottomDigit = 0;
            }
            
            //  Compare top/bottom digit magnitudes and borrow from next digit if necessary.
            if (topSign == 1 && bottomSign == -1 && topSign * topDigit - bottomSign * bottomDigit < 0) {
                borrow = 1;
                topDigit += topSign * base;
            }
            
            //  Do the addition.
            current = topDigit + bottomDigit;
            
            //  Take the magnitude of the final digit.
            if (current < 0) { 
                current *= -1;
            }
            
            //  Add any carry.
            current += carry;
            
            //  Place any carry if we're over the base.
            if (current >= base) {
                carry = 1;
                current %= base;
            }
            
            //  Append the final digit.
            result.append(digitToString(current));
        }
        
        //  Add any final carry.
        if (carry != 0) {
            result.append(carry);
        }
        
        //  Since we've ordered the largest number on top, only a top-negative and bottom-
        //  positive number can result in a negative number.  If the absolute value of the
        //  numbers is the same, then we get zero - do not append the negative character.
        if ((topSign    == -1 && stringNumberCompareTo(top, bottom) ==  1) ||
            (bottomSign == -1 && stringNumberCompareTo(top, bottom) == -1)    ) {
            
            result.append(negativeSign);
        }
    
        String finalResult = reverse(result.toString());
        
        //  Remove any leading zeroes.
        finalResult = finalResult.replaceFirst("0*", "");
        
        if (finalResult.isEmpty()) {
            return "0";
        } else {
            return finalResult;
        }
    }
    
    public static class StringDividend {
        public String dividend;
        public String remainder;
    }
    
    public static int charToDigit(final char digit) {
        if (digit >= '0' && digit <= '9') {
            return digit - '0';
        } else if (digit >= 'A' && digit <= 'Z') {
            return digit - 'A' + 10;
        } else if (digit >= 'a' && digit <= 'z') {
            return digit - 'a' + 10;
        } else {
            throw new IllegalArgumentException("Digit values must be either 0...9,A...Z");
        }
    }
    
    public static String digitToString(final int number) {
        if (number > 36) {
            throw new IllegalArgumentException("Digit values must be less than 36.");
        }
        
        if (number > 9) {
            return String.valueOf((char)('A' - 10 + number));
        } else {
            return String.valueOf(number);
        }
    }
    
    public static int stringNumberCompareTo(final String left, final String right) {
        String negativeSign = String.valueOf(StringOperations.decimalFormat.getDecimalFormatSymbols().getMinusSign());
        
        int leftSign = 1;
        int rightSign = 1;
        
        String cleanLeft;
        if (left.charAt(0) == negativeSign.charAt(0)) {
            cleanLeft = left.replace(negativeSign, "");
            leftSign = -1;
        } else {
            cleanLeft = left;
        }
        
        String cleanRight;
        if (right.charAt(0) == negativeSign.charAt(0)) {
            cleanRight = right.replace(negativeSign, "");
            rightSign = -1;
        } else {
            cleanRight = right;
        }
        
        int leftDigit;
        int rightDigit;
        
        if (cleanLeft.length() < cleanRight.length() || leftSign < rightSign) {
            return -1;
        } else if (cleanLeft.length() > cleanRight.length() || leftSign > rightSign) {
            return 1;
        } else {
            for (int i=0; i<cleanLeft.length(); i++) {
                leftDigit = cleanLeft.charAt(i) - '0';
                rightDigit = cleanRight.charAt(i) - '0';
                
                if (leftDigit < rightDigit) {
                    return -1;
                } else if (leftDigit > rightDigit) {
                    return 1;
                }
            }
            
            return 0;
        }
    }
    
    public static String reverse(final String value) {
        StringBuilder builder = new StringBuilder();
        
        for (int i=0; i<value.length(); i++) {
            builder.append(value.charAt(value.length()-i-1));
        }
        
        return builder.toString();
    }
    
    public static Collection<String> allSubStrings(final String original) {
        
        //  This same approach can be easily generalized to arbitrary sets.
        Deque<String> results = new ArrayDeque<>();
        
        if (original.isEmpty()) {
            return results;
        }
        
        results.add(new String(new char[] { original.charAt(0) }));
        
        for (int i=1; i<original.length(); i++) {
            char character = original.charAt(i);
            
            do {
                String current = results.pop();
                
                for (int j=0; j<=current.length(); j++) {
                    //  This approach is super space-ineffecient for large strings as every
                    //  string will be recreated from scratch every time for every size.
                    //  Using char[]s presized to the final lengths would be much better.
                    results.addLast(insertCharAt(current, character, j));
                }
            } while (results.peek().length() == i);
        }
        
        return results;
    }
    
    public static String insertCharAt(final String original, final char c, int index) {
        StringBuilder result = new StringBuilder();
        if (index > 0) {
            result.append(original.substring(0, index));
        }
        result.append(c);
        if (index < original.length()) {
            result.append(original.substring(index, original.length()));
        }
        return result.toString();
    }
}
