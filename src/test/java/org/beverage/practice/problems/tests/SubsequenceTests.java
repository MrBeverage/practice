package org.beverage.practice.problems.tests;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class SubsequenceTests {

    @Test(dataProvider = "testCases")
    public <T extends Comparable<T>> void foo(final T[] seq1, final T[] seq2, final T[] expectedResult) {
        List<T> result = lcs(seq1, seq2);
        
        assertThat(result, contains(expectedResult));
    }
    
    @DataProvider
    public Object[][] testCases() {
        return new Object[][] {
                { new String[] { "A", "G", "C", "A", "T" }, new String[] { "G", "A", "C" }, new String[] { "G", "A" } },
                { new String[] { "G", "A", "C" }, new String[] { "A", "G", "C", "A", "T" }, new String[] { "A", "C" } },
                { new Integer[] { 1, 2, 3, 4 }, new Integer[] { 2, 3 }, new Integer[] { 2, 3 } }
        };
    }
    
    public <T extends Comparable<T>> List<T> lcs(T[] seq1, T[] seq2) {
        int[][] table = buildLCSTable(seq1, seq2);
        
        return lcsInternal(table, seq1, seq2, seq2.length, seq1.length);
    }
    
    private <T extends Comparable<T>> List<T> lcsInternal(int[][] lcsTable, T[] seq1, T[] seq2, int i, int j) {
        
        if (i == 0 || j == 0) {
            return new ArrayList<T>();
        } else if (seq1[j-1].compareTo(seq2[i-1]) == 0) {
            List<T> temp = lcsInternal(lcsTable, seq1, seq2, i-1, j-1);
            temp.add(seq1[j-1]);
            return temp;
        } else {
            if (lcsTable[i][j-1] > lcsTable[i-1][j]) {
                return lcsInternal(lcsTable, seq1, seq2, i, j-1);
            } else {
                return lcsInternal(lcsTable, seq1, seq2, i-1, j);
            }
        }
    }
    
    public <T extends Comparable<T>> int[][] buildLCSTable(T[] seq1, T[] seq2) {
        int[][] lcs = new int[seq2.length+1][seq1.length+1];
        
        for (int i=1; i<=seq2.length; i++) {
            for (int j=1; j<=seq1.length; j++) {
                lcs[i][j] = max(lcs[i-1][j], lcs[i][j-1]);
                
                if (seq1[j-1].compareTo(seq2[i-1]) == 0) {
                    lcs[i][j]++;
                }
            }
        }
        
        return lcs;
    }
    
    public <T extends Comparable<T>> T max(T l, T r) {
        return l.compareTo(r) > 0 ? l : r;
    }
}
