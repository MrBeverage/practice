package org.beverage.practice.sorting;

public interface PivotSelector<T extends Comparable<? super T>> {
    int selectPivot(Object[] items, int lowerBound, int upperBound);
    
    //  Because Collections are not Comparable, even though the tree of sorting classes
    //  require that elements are comparable, due to type erasure that constraint is lost
    //  when working with the Collection's methods leading to ClassCastExceptions left
    //  and right.  We keep the generic declaration to indicate this requirement, however
    //  we cannot enforce it in the method signature.
}
