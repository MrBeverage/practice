package org.beverage.practice.graphing.tests;

import java.security.SecureRandom;
import java.util.Collection;

import org.beverage.practice.graphs.EdgeMapEdge;
import org.beverage.practice.graphs.EdgeMapGraph;
import org.beverage.practice.graphs.EdgeMapGraphNode;
import org.testng.annotations.Test;

import static org.beverage.practice.graphing.tests.GraphTestData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * This are mostly for exploring the graph test data generators.  Only one of these
 * test methods is an actual test.
 * 
 * @author alexbeverage
 *
 */
public class GraphGenerationTests {

    private static final SecureRandom random = new SecureRandom();
    
    @Test
    public void weightedGraph() {
        Integer[][] graph = new Integer[10][10];
        
        fillRandomConnectionMatrix(30, graph, () -> random.nextInt(9) + 1);
        
        System.out.println(toStringMatrix(graph));
        
        System.out.println(toStringConnections(graph));
    }
    
    @Test
    public void addNodesWithEdges() {
        EdgeMapGraph<Integer, Integer> graph = new EdgeMapGraph<>();
        int listSize = 100;
        int edgeCount = 5;
        
        for (int i=0; i<listSize; i++) {
            graph.addNode(new EdgeMapGraphNode<>(i));
        }
        
        for (int i=0; i<listSize; i++) {
            while (graph.getEdges(i).size() < edgeCount) {
                //  This approach is fairly safe for sparse graphs.
                graph.addEdge(i, random.nextInt(listSize));
            }
        }
        
        assertThat(graph.size(), equalTo(listSize));
        
        for(EdgeMapGraphNode<Integer> node : graph) {
            assertThat(graph.getEdges(node.getValue()).size(), equalTo(edgeCount));
        }
    }
    
    @Test
    public void buildEdgeCostGraph() {
        int expectedGraphSize = 100;
        int expectedEdgeCount = 10;
        int expectedCostRange = 10;
        
        //  Random call complexity is to make sure we don't generate zero.
        EdgeMapGraph<Integer, Integer> graph = buildIntegerEdgeCostMap(
                expectedGraphSize, expectedEdgeCount, () -> random.nextInt(expectedCostRange - 1) + 1);
        
        assertThat(graph.size(), equalTo(expectedGraphSize));
        
        for(EdgeMapGraphNode<Integer> node : graph) {
            Collection<EdgeMapEdge<Integer, Integer>> edges = graph.getEdges(node.getValue());
            assertThat(edges, hasSize(expectedEdgeCount));
            
            for (EdgeMapEdge<Integer, Integer> edge : edges) {
                assertThat(edge.getLeft(),  equalTo(node.getValue()));
                assertThat(edge.getRight(), not(equalTo(node.getValue())));
                
                assertThat(edge.getValue(), greaterThan(0));
                assertThat(edge.getValue(), lessThanOrEqualTo(expectedCostRange));
            }
        }
    }
}
