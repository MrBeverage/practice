package org.beverage.practice.async.tests;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.concurrent.TimeoutException;

import lombok.Data;

import org.beverage.practice.async.Dispatcher;
import org.beverage.practice.async.ReverseStringRequest;
import org.beverage.practice.primitives.StringOperations;
import org.testng.annotations.Test;

public class DispatcherTests {
    
    @Data
    public static class ResponseContainer<T> {
        T value;
        Exception error;
        
        public ResponseContainer(final T value) {
            this.value = value;
        }
    }
    
    @Test
    public void dispatchSingleJob() throws InterruptedException {
        final String request = "Hi!";
        final Dispatcher dispatcher = new Dispatcher(1000, 250);
        final ResponseContainer<String> responseContainer = new ResponseContainer<>(request);
        
        ReverseStringRequest requestor = new ReverseStringRequest(0) {
            @Override
            public void handleResponse(String response) {
                responseContainer.setValue(response);
            }
        };
        
        dispatcher.dispatch(request, requestor);
        
        while (dispatcher.hasJobs()) {
            Thread.sleep(100);
        }
        
        assertThat(responseContainer.getValue(), equalTo(StringOperations.reverse(request)));
        
        dispatcher.close();
    }
    
    @Test
    public void singleJobTimesOut() throws InterruptedException {
        final String request = "Hi!";
        final Dispatcher dispatcher = new Dispatcher(1000, 250);
        final ResponseContainer<String> responseContainer = new ResponseContainer<>(request);
        
        ReverseStringRequest requestor = new ReverseStringRequest(2000) {
            @Override
            public void handleResponse(String response) {
                responseContainer.setValue(response);
            }
            
            @Override
            public void handleError() {
                responseContainer.setError(new TimeoutException());
            }
        };
        
        dispatcher.dispatch(request, requestor);
        
        while (dispatcher.hasJobs()) {
            Thread.sleep(100);
        }
        
        //assertThat(responseContainer.getValue(), equalTo(nullValue()));
        assertThat(responseContainer.getError(), not(nullValue()));
        assertThat(responseContainer.getError().getClass(), equalTo(TimeoutException.class));
        
        dispatcher.close();
    }
}
