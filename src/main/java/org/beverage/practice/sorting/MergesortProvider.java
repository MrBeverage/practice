package org.beverage.practice.sorting;

import org.beverage.practice.collections.Collection;

public class MergesortProvider<T extends Comparable<? super T>> implements SortProvider<T> {

    @SuppressWarnings("unchecked")
    @Override
    public void sort(Collection<T> collection) {
        if (collection == null || collection.size() < 2) {
            return;
        }
        
        Object[] items = collection.toArray();
        Object[] sortedArray = new Object[items.length];
        sortInternal(items, sortedArray, 0, collection.size() - 1);
        collection.clear();
        for (Object o : items) {
            collection.add((T) o);
        }
        sortedArray = null;
    }

    @Override
    public void sort(T[] items) {
        if (items == null || items.length < 2) {
            return;
        }
        
        Object[] sortedArray = new Object[items.length];
        sortInternal(items, sortedArray, 0, items.length - 1);
        sortedArray = null;
    }

    private void sortInternal(Object[] items, Object[] sortedArray, int lowerBound, int upperBound) {
        
        int range = upperBound - lowerBound;
        
        //  bound == 0 - sorted:
        if (range <= 0) {
            return;
        }
        
        //  bound == 1 - check if a swap is needed:
        if (range == 1) {
            if (SortHelper.objectCompareTo(items[lowerBound], items[upperBound]) == 1) {
                SortHelper.swapItems(items, lowerBound, upperBound);
            }
            return;
        }
        
        //  bound > 2 - split and merge:
        int midpoint = lowerBound + range / 2;
        
        sortInternal(items, sortedArray, lowerBound, midpoint);
        sortInternal(items, sortedArray, midpoint+1, upperBound);
        
        int lowerCursor = lowerBound;
        int upperCursor = midpoint+1;
        
        int i=lowerBound;
        
        while (i <= upperBound) {
            if (lowerCursor <= midpoint && (upperCursor > upperBound || 
                    SortHelper.objectCompareTo(items[lowerCursor], items[upperCursor]) == -1)) {
                
                sortedArray[i++] = items[lowerCursor++];
            } else {
                sortedArray[i++] = items[upperCursor++];
            }
        }
        
        System.arraycopy(sortedArray, lowerBound, items, lowerBound, range+1);
    }
}
