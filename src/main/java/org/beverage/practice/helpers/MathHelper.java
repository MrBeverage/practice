package org.beverage.practice.helpers;

import java.security.SecureRandom;

public class MathHelper {

    private static SecureRandom random = new SecureRandom();
    
    public static int nextRandomPositiveInt(int bound) {
        int result = random.nextInt(bound) ;
        return result;
    }
}
