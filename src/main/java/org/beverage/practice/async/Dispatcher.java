package org.beverage.practice.async;

import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

import lombok.AllArgsConstructor;

public class Dispatcher implements AutoCloseable {

    @AllArgsConstructor
    private static class ThreadState {
        private final Thread thread;
        private final long startTime;
        private final boolean isErrorHander;
    }
    
    private final long timeoutMillis;
    private final long timeoutCheckInterval;
    private final ConcurrentHashMap<Request<?>, ThreadState> threads;
    private final Thread timeoutManagerThread;
    
    public Dispatcher(long timeoutMillis, long timeoutCheckInterval) {
        this.timeoutMillis = timeoutMillis;
        this.timeoutCheckInterval = timeoutCheckInterval;
        this.threads = new ConcurrentHashMap<>();
        try {
            this.timeoutManagerThread = initTimeoutManager();
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to start timeout manager thread.", e);
        }
        
        this.timeoutManagerThread.start();
    }
    
    private Thread initTimeoutManager() throws InterruptedException {
        
        Runnable timeoutManagerRunnable = new Runnable() {
            @Override
            public void run() {
                while (true) {
                    long lastCheckStartTime = System.currentTimeMillis();
                    
                    Iterator<Request<?>> requestorIterator = threads.keySet().iterator();
                    
                    while (requestorIterator.hasNext()) {
                        Request<?> requestor = requestorIterator.next();
                        ThreadState state = threads.get(requestor);
                        
                        if (lastCheckStartTime - state.startTime > timeoutMillis) {
                            state.thread.interrupt();
                            
                            //  If this is a worker job, run it's error handler.
                            if (state.isErrorHander == false) {
                                Runnable errorRunnable = new Runnable() {
                                    @Override
                                    public void run() {
                                        requestor.handleError();
                                    }
                                };
                                
                                Thread errorHandlerThread = new Thread(errorRunnable);
                                ThreadState errorHandlerThreadState = new ThreadState(errorHandlerThread, System.currentTimeMillis(), true);
                                threads.put(requestor, errorHandlerThreadState);
                                errorHandlerThread.start();
                            }
                            
                            requestorIterator.remove();
                        }
                    }
                    
                    //  Sleep for any remaining gap in the timeout interval not used.
                    long timeDelta = System.currentTimeMillis() - lastCheckStartTime;
                    try {
                        Thread.sleep(timeoutCheckInterval - timeDelta);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        
        Thread timeoutManagerThread = new Thread(timeoutManagerRunnable);
        return timeoutManagerThread;
    }
    
    public <T> void dispatch(final T request, final Request<T> requestor) {
        
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                T response = requestor.execute(request);
                threads.remove(requestor);
                requestor.handleResponse(response);
            }
        };
        
        Thread thread = new Thread(runnable);
        ThreadState state = new ThreadState(thread, System.currentTimeMillis(), false);
        threads.put(requestor, state);
        thread.start();
    }
    
    public boolean hasJobs() {
        return threads.size() > 0;
    }

    @Override
    public void close() {
        this.timeoutManagerThread.interrupt();
        
        for (Request<?> request : this.threads.keySet()) {
            threads.get(request).thread.interrupt();
        }
        
        threads.clear();
    }
}
