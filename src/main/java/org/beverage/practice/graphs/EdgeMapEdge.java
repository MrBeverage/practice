package org.beverage.practice.graphs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * An edge-with-value implementation for graphs.  The value object in most cases
 * should be some sort of object describing a traversal cost.
 * 
 * @author alexbeverage
 *
 * @param <C>
 */
@Data
@Accessors(chain = true)
@AllArgsConstructor
public class EdgeMapEdge<NodeType, WeightType> {
    public NodeType left;
    public NodeType right;
    public WeightType value;
    
    public EdgeMapEdge(NodeType leftNode, NodeType rightNode) {
        this.left = leftNode;
        this.right = rightNode;
    }
}
