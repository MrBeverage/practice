package org.beverage.practice.problems.tests;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

import lombok.Getter;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class StackProblems {

    //  This may have just made things more tedious.
    enum Operator {
        ADD("+"),
        SUBTRACT("-"),
        MULTIPLY("*"),
        DIVIDE("/"),
        LEFT_PAREN("("),
        RIGHT_PAREN(")");
        
        @Getter private final String value;
        
        private Operator(String operation) {
            this.value = operation;
        }
    }
    
    @Test(dataProvider = "calculatorProblems")
    public void calculator(final long expectedResult, final String equation) {
        String[] tokens = equation.split(" ");
        
        Deque<String> operations = new ArrayDeque<String>();
        List<Long> digits = new ArrayList<Long>();
        
        int i=0;
        
        do {
            if (i < tokens.length) {
                while (i < tokens.length && tokens[i].equals(Operator.RIGHT_PAREN.getValue()) == false) {
                    operations.push(tokens[i++]);
                }
            }
            
            digits.clear();
            
            //  Bad assumption - numbers cannot be negative for this to work.
            while (isDigit(operations.peek().charAt(0))) {
                digits.add(Long.parseLong(operations.pop()));
            }
            
            Long result = performOperation(operations.pop(), digits);
            
            //  Check for matching paren.
            if (operations.size() > 0 && operations.peek().equals(Operator.LEFT_PAREN.getValue())) {
                operations.pop();
            }
            
            operations.push(result.toString());
            i++;
            
        } while (operations.size() > 1);
        
        Long result = Long.parseLong(operations.pop());
        assertThat(result, equalTo(expectedResult));
    }
    
    private boolean isDigit(char value) {
        return value >= '0' && value <= '9';
    }
    
    private long performOperation(String operator, List<Long> arguments) {
        if (arguments.size() == 0) {
            return 0l;
        }
        
        long result = arguments.get(0);
        
        for (int i=1; i<arguments.size(); i++) {
            long operand = arguments.get(i);
            
            if (operator.equals(Operator.ADD.getValue())) {
                result += operand;
            } else if (operator.equals(Operator.SUBTRACT.getValue())) {
                result -= operand;
            } else if (operator.equals(Operator.MULTIPLY.getValue())) {
                result *= operand;
            } else if (operator.equals(Operator.DIVIDE.getValue())) {
                result /= operand;
            } else {
                //  Something that makes more sense later.
                throw new RuntimeException("Invalid operation: " + operator);
            }
        }
        
        return result;
    }
    
    @DataProvider
    public Object[][] calculatorProblems() {
        return new Object[][] {
                { 1,   "( + 0 1 )" },
                { 1,   "+ 0 1" },
                { 1,   "* 1 1" },
                { 0,   "* 0 1" },
                { 6,   "+ 2 4" },
                { 152, "* 8 ( + 7 12 )" },
                { 288, "( + 7 ( * 8 12 ) ( * 2 ( + 9 4 ) 7 ) 3 )" },
        };
    }
}
