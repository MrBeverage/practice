package org.beverage.practice.graphs;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/*
 * Extension of DirectedGraph that can return the nodes in topological order.
 * The name is a bit of a lie as the object does not enforce acyclicness on add as that's
 * a more difficult problem to solve in reasonable time for this problem, and is not 
 * necessary for the solution.
 */
public class DirectedAcyclicGraph<NodeValue, EdgeValue> extends DirectedGraph<NodeValue, EdgeValue> {

    public Deque<NodeValue> getTopologicalOrdering() {
        
        //  Space complexity: O(n).
        //  While we'll need to construct something that contains a reference to every cell in 
        //  topological order, these should just be indexes into structures containing all the 
        //  real data/metadata meaning we're duplicating only the minimal set to look things
        //  up in order later.
        Deque<NodeValue> allNodes   = new ArrayDeque<>(nodes.keySet());
        Deque<NodeValue> workingSet = new ArrayDeque<>();
        Map<NodeValue, Boolean> visitedMap = new HashMap<>();
        
        Deque<NodeValue> result = new ArrayDeque<>();
        
        //  This loop construct is ugly.  Can probably simplify this.
        outer:
        while (allNodes.isEmpty() == false) {
            
            //  If the working set is empty, find a new tree head node to explore from.
            while (workingSet.isEmpty()) {
                if (allNodes.size() > 0) {
                    NodeValue newHead = allNodes.pop();
                    if (visitedMap.get(newHead) == null) {
                        workingSet.push(newHead);
                    }
                } else {
                    //  We're done!
                    break outer;
                }
            }
            
            NodeValue current = workingSet.peek();
            int workingSetPriorSize = workingSet.size();
            
            //  null == never reached, false == reached and diving, true == reached and cleared.
            if (visitedMap.get(current) == null) {
                visitedMap.put(current, false);
                
                Set<NodeValue> children = reverseLookup.get(current).keySet();
                
                for (NodeValue child : children) {
                    if (visitedMap.get(child) == null) {
                        workingSet.push(child);
                    } else if (visitedMap.get(child) == false) {
                        //  A more thorough implementation could walk backwards through
                        //  the tree and return the entire cycle.
                        throw new IllegalStateException("Cycle detected: " + current);
                    }
                }
            }

            if (workingSet.size() == workingSetPriorSize) {
                //  No valid children to add, mark this node as complete and pop it.
                visitedMap.put(current, true);
                result.push(workingSet.pop());
            }
        }
        
        return result;
    }
    
    /**
     * Return current topologically sorted list of nodes.
     */
    @Override
    public Iterator<NodeValue> iterator() {
        //  This is very inefficient to recompute every time, even if the algorithm above is roughly
        //  O(n).  A good next step might be to overload all the add/remove/etc methods to set a 
        //  dirty flag, and only recompute the topological ordering when requested after changes.
        //
        //  Another good option would be to replace the sort entirely with a data structure
        //  capable of managing the topological ordering through updates.  A quick google search
        //  shows structures like this exist with O(n^1/2) on add/remove.
        //
        //  This works for the purposes of solving this problem in the time allotted.
        //
        return getTopologicalOrdering().iterator();
    }
}
