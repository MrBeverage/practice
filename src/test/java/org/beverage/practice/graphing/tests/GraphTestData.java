package org.beverage.practice.graphing.tests;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

import org.beverage.practice.datafactory.RandomObjectFactory;
import org.beverage.practice.datafactory.SequentialIntegerFactory;
import org.beverage.practice.graphs.EdgeMapGraph;
import org.beverage.practice.graphs.EdgeMapGraphNode;

public class GraphTestData {

    private static final SecureRandom random = new SecureRandom();
    
    public static <T> String toStringMatrix(T[][] matrix) {
        StringBuilder builder = new StringBuilder();

        for (int i=0; i<matrix.length; i++) {
            for (int j=0; j<matrix[i].length; j++) {
                builder.append(matrix[i][j]);
                if (j != matrix[i].length - 1) {
                    builder.append(", ");
                }
            }
            builder.append("\n");
        }
        
        return builder.toString();
    }
    
    public static <T> String toStringConnections(T[][] graphMatrix) {
        StringBuilder builder = new StringBuilder();
        
        for (int i=0; i<graphMatrix.length; i++) {
            builder.append(i + ": ");
            
            for (int j=0; j<graphMatrix[i].length; j++) {
                if (graphMatrix[i][j] != null) {
                    builder.append(j + "[" + graphMatrix[i][j] + "]");
                    
                    if (j != graphMatrix[i].length - 1) {
                        builder.append(", ");
                    }
                }
            }

            builder.append("\n");
        }
        
        return builder.toString();
    }
    
    public static <T> void fillRandomConnectionMatrix(
            final int density, final T[][] graph, final RandomObjectFactory<? extends T> factory) {
        
        if (density < 0 || density > 100) {
            throw new IllegalArgumentException("density is out of bounds: " + density);
        }
        
        int size = graph.length;
        
        for (int i=0; i<size; i++) {
            if (graph.length != graph[i].length) {
                throw new IllegalArgumentException("connection graph must be square");
            }
            
            for (int j=i+1; j<size; j++) {
                if (random.nextInt(100) < density) {
                    graph[i][j] = factory.next();
                    graph[j][i] = graph[i][j];
                }
            }
        }
    }
    
    @Data
    @AllArgsConstructor
    public class GraphConnection<T> {
        T value;
        int nodeIndex;
    }
    
    public static <T> void fillConnectionMatrix(final GraphConnection<T>[][] connections, final T[][] outputGraph) {
        
        if (connections == null) {
            throw new IllegalArgumentException("connections graph is null");
        }
        
        if (outputGraph == null) {
            throw new IllegalArgumentException("outputGraph is null");
        }
        
        if (connections.length != outputGraph.length || connections.length != outputGraph[0].length) {
            throw new IllegalArgumentException("connection graph to output graph size mismatch");
        }
        
        for (int i=0; i<connections.length; i++) {
            for (GraphConnection<T> connection : connections[i]) {
                outputGraph[i][connection.nodeIndex] = connection.value;
            }
        }
    }
    
    /**
     * Builds a random sparse graph with edge values.  The parameters do not have to describe a sparse graph, 
     * but the algorithm used to fill the graph is very simple and could take a long time.
     * 
     * @param graphSize The number of nodes in the graph.
     * @param edgeCountPerNode The number of edges each node should have.
     * @param edgeCostFactory Edge value generator function.
     * @return The random graph.
     */
    public static <N> EdgeMapGraph<Integer, Integer> buildIntegerEdgeCostMap(
            final int graphSize, final int edgeCountPerNode, final RandomObjectFactory<Integer> edgeCostFactory) {
        
        return buildSimpleSparseGraph(graphSize, edgeCountPerNode, new SequentialIntegerFactory(), edgeCostFactory);
    }
    
    /**
     * Builds a random graph with sequential integer node values.  The parameters do not have to describe a sparse graph, 
     * but the  algorithm used to fill the graph is very simple and could take a long time.
     * @param graphSize The number of nodes in the graph.
     * @param edgeCountPerNode The number of edges each node should have.
     * @return The random graph.
     */
    public static EdgeMapGraph<Integer, Object> buildSequentialNodeValueGraph(
            final int graphSize, final int edgeCountPerNode) {
        
        return buildSimpleSparseGraph(graphSize, edgeCountPerNode, new SequentialIntegerFactory(), () -> null);
    }
    
    /**
     * Builds a random sparse graph with no node/edge values.  The parameters do not have to describe a sparse 
     * graph, but the algorithm used to fill the graph is very simple and could take a long time.
     * 
     * @param graphSize The number of nodes in the graph.
     * @param edgeCountPerNode The number of edges each node should have.
     * @return The random graph.
     */
    public static <N, E> EdgeMapGraph<N, E> buildSimpleSparseGraph(
            final int graphSize, final int edgeCountPerNode) {
        
        return buildSimpleSparseGraph(graphSize, edgeCountPerNode, () -> null, () -> null);
    }
    
    /**
     * Generic sparse graph generator.  The parameters do not have to describe a sparse graph, but the 
     * algorithm used to fill the graph is very simple and could take a long time.
     * 
     * @param graphSize The number of nodes in the graph.
     * @param edgeCountPerNode The number of edges each node should have.
     * @param nodeValueFactory Node value generator.
     * @param edgeValueFactory Edge value generator.  If null, edges will be generated with null values.
     * @return The random graph.
     */
    public static <N, E> EdgeMapGraph<N, E> buildSimpleSparseGraph(
            final int graphSize, final int edgeCountPerNode,
            final RandomObjectFactory<N> nodeValueFactory, 
            final RandomObjectFactory<E> edgeValueFactory) {
        
        if (graphSize < 1) {
            throw new IllegalArgumentException("Graph size must be 1 or greater.");
        }
        if (edgeCountPerNode >= graphSize) {
            throw new IllegalArgumentException("Nodes cannot contain more edges than there are other nodes.");
        }
        if (nodeValueFactory == null) {
            throw new IllegalArgumentException("A node factory is required.");
        }
        
        EdgeMapGraph<N, E> graph = new EdgeMapGraph<>();
        
        for (int i=0; i<graphSize; i++) {
            graph.addNode(nodeValueFactory.next());
        }
        
        List<EdgeMapGraphNode<N>> nodes = new ArrayList<EdgeMapGraphNode<N>>(graph.getNodes());
        
        for (EdgeMapGraphNode<N> node : graph) {
            N leftValue = node.getValue();
            
            while (graph.getEdges(leftValue).size() < edgeCountPerNode) {
                //  Randomly select a node to connect to that has not already been chosen, and is
                //  not the current node.  This could take a long time if the parameters describe
                //  a dense graph.  The problems I am trying to solve now use sparse graphs so this
                //  will be adequate.
                int randomNodeIndex = random.nextInt(graphSize);
                N rightValue = nodes.get(randomNodeIndex).getValue();
                
                if (leftValue.equals(rightValue) == false && graph.hasEdge(leftValue, rightValue) == false) {
                    graph.addEdge(leftValue, rightValue, edgeValueFactory == null ? null : edgeValueFactory.next());
                }
            }
        }
        
        return graph;
    }
}
