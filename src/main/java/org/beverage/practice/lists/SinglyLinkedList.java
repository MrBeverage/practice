package org.beverage.practice.lists;

import java.util.Iterator;

import org.beverage.practice.collections.Collection;

public class SinglyLinkedList<T> extends AbstractList<T> {

    public SinglyLinkedList() {
        super();
    }

    @Override
    public void add(T value) {
        
        BasicListNode<T> end = this.getNode(this.size() - 1);
        
        BasicListNode<T> newNode = new BasicListNode<>();
        newNode.value = value;
        
        incrementRevisionNumber();
        
        if (end == null) {
            setHeadNode(newNode);
        } else {
            end.next = newNode;
        }
        
        incrementSize();
    }
    
    @Override
    public void addAll(T[] items) {
        incrementRevisionNumber();
        
        for (T item : items) {
            add(item);
        }
    }
    
    @Override
    public void addAll(Collection<T> items) {
        incrementRevisionNumber();
        
        for (T item : items) {
            add(item);
        }
    }
    
    @Override
    public void insert(T value, int index) {
        if (index > size()) {
            throw new IllegalArgumentException("Index is greater than the List's size.");
        }
        
        BasicListNode<T> previous = null;
        BasicListNode<T> current  = getHeadNode();

        BasicListNode<T> newNode  = new BasicListNode<>();
        newNode.value = value;
        
        //  If current is null, this is the first node.
        incrementRevisionNumber();
        
        if (current == null) {
            setHeadNode(newNode);
        } else {
            for (int i=0; i<index; i++) {
                previous = current;
                current = current.next;
            }
            
            if (previous == null) {
                newNode.next = current;
                setHeadNode(newNode);
            } else {
                previous.next = newNode;
                newNode.next  = current;
            }
        }
        
        incrementSize();
    }

    @Override
    public boolean remove(T value) {
        Iterator<T> iterator = iterator();
        T item = null;
        
        while (iterator.hasNext()) {
            item = iterator.next();
            if (item.equals(value)) {
                iterator.remove();
                return true;
            }
        }
        
        return false;
    }

    @Override
    public boolean remove(int index) {
        if (index > size()) {
            throw new IllegalArgumentException("Index is greater than the List's size.");
        }
        
        T value = get(index);
        return value == null ? false : remove(value);
    }
    
    @Override
    public T get(int index) {
        if (index >= size()) {
            throw new IllegalArgumentException("Index is greater than the List's size.");
        }
        
        BasicListNode<T> current = getNode(index);
        return current.value;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public T[] toArray() {
        Object[] elements = new Object[size()];
        
        int i = 0;
        for (T item : this) {
            elements[i++] = item;
        }
        
        return (T[]) elements;
    }
    
    @Override
    public T[] toArray(T[] array) {
        if (array.length < size()) {
            throw new IllegalArgumentException("Destination array capacity is to low: " + array.length + ", need " + size());
        }
        
        int i = 0;
        for (T item : this) {
            array[i++] = item;
        }
        
        return array;
    }
    
    @Override
    protected BasicListNode<T> getNode(int index) {
        if (index > size()) {
            throw new IllegalArgumentException("Index is greater than the List's size.");
        }
        
        BasicListNode<T> current = getHeadNode();
        for (int i=0; i<index; i++) {
            current = current.next;
        }
        
        return current;
    }
}
