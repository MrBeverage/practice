package org.beverage.practice.graphs;

import java.util.Collection;

public interface Graph<NodeValue, EdgeValue> {

    int getSize();
    
    boolean contains(NodeValue node);
    
    Collection<NodeValue> getNodes();
    Collection<NodeValue> getEdges(NodeValue node);
    EdgeValue getEdgeValue(NodeValue left, NodeValue right);
    
    Graph<NodeValue, EdgeValue> addNode(NodeValue n);
    Graph<NodeValue, EdgeValue> addNodes(Collection<NodeValue> nodes);
    Graph<NodeValue, EdgeValue> addEdge(NodeValue left, NodeValue right, EdgeValue edge);
    
    Graph<NodeValue, EdgeValue> removeNode(NodeValue n);
    Graph<NodeValue, EdgeValue> removeNodes(Collection<NodeValue> nodes);
    Graph<NodeValue, EdgeValue> removeEdge(NodeValue left, NodeValue right);
    
    Graph<NodeValue, EdgeValue> updateNode(NodeValue oldValue, NodeValue newValue);
}
