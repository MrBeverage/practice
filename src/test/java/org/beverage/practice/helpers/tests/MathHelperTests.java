package org.beverage.practice.helpers.tests;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.beverage.practice.helpers.MathHelper;
import org.testng.annotations.Test;

public class MathHelperTests {

    @Test(enabled = false, invocationCount = 10000)
    public void randomIntegerGenerator() {
        int number = MathHelper.nextRandomPositiveInt(10);
        assertThat(number, greaterThan(0));
        assertThat(number, lessThan(11));
    }
}
