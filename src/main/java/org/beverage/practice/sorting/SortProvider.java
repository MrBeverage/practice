package org.beverage.practice.sorting;

import org.beverage.practice.collections.Collection;

public interface SortProvider<T extends Comparable<? super T>> {
    //  Sadly we can't make these methods fluent, as Collection<T> doesn't require that
    //  T be Comparable.  Since the SortProvider's do, and even though T in both cases
    //  is identical, this information has already be lost.  Because of that, we cannot
    //  cast Collection's T to the SortProvider's T - only to Object.  In place sorting
    //  on T[] would work fine fluently, we cannot put those items back into a collection
    //  so to keep the interfaces consistent neither will be fluent.
    void sort(Collection<T> collection);
    void sort(T[] items);
}
